<?php
Route::get('/', function () { 
	return redirect()->route('dashboard'); 
});

//custom redirect if page not found
Route::get('error_page_redirect', 'HomeController@error_page_redirect')->name('error_page_redirect');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
Route::post('login', 'Auth\LoginController@login')->name('auth.login');
Route::post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    Route::group(['middleware' => ['authorize','auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/home', 'HomeController@index');

        // For Permission
        Route::resource('permissions', 'Admin\PermissionsController');
        Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);

        // For Roles
        Route::resource('roles', 'Admin\RolesController');
        Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);

        // For Users
        Route::resource('users', 'Admin\UsersController');
        Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);

    });
});

Route::group([ 'middleware' => 'auth' ], function () {
    // ...
    Route::get('/notifications', 'Admin\UsersController@notifications');
});

Route::prefix('sadmin')->group(function () {

  Route::get('/', 'AdminController@index')->name('admin.dashboard');
  Route::get('dashboard', 'AdminController@index')->name('admin.dashboard');
  Route::get('register', 'AdminController@create')->name('admin.register');
  Route::post('register', 'AdminController@store')->name('admin.register.store');
  Route::get('login', 'Auth\Admin\LoginController@login')->name('admin.auth.login');
  Route::post('login', 'Auth\Admin\LoginController@loginAdmin')->name('admin.auth.loginAdmin');
  Route::post('logout', 'Auth\Admin\LoginController@logout')->name('admin.auth.logout');

});
Route::group(['middleware' => ['auth']], function () {
    Route::get('/authorize/status', [
        'name' => 'Authorize Login',
        'as' => 'authorize.status',
        'uses' => 'AuthorizeController@status',
    ]);

    Route::post('/authorize/verify', [
        'name' => 'Authorize Login',
        'as' => 'authorize.verify',
        'uses' => 'AuthorizeController@verify',
    ]);
    Route::post('/authorize', [
        'name' => 'Authorize Login',
        'as' => 'authorize.verify_message',
        'uses' => 'AuthorizeController@verify_message',
    ]);
    Route::get('/authorized', [
        'name' => 'Authorize Login',
        'as' => 'authorize.authorized',
        'uses' => 'AuthorizeController@authorized',
    ]);
    Route::put('authorized/{id}', array('as' => 'authorized.update', 'uses' => 'AuthorizeController@authorized_update'));

});


