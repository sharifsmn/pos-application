<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Theme;
use App\User;
use Auth;
use Modules\Core\Entities\Floor;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $user = Auth::user();
        
        if($user->hasRole('waiter')){
            Theme::uses('front')->layout('master.layout');
            $floors = Floor::where('vendor_id',1)->where('location_id', \Auth::user()->location_id )->get();
            return Theme::view('frontend.table.index', compact('floors'));
        }else{
            Theme::uses('default')->layout('master.layout');
            return Theme::view('core.dashboard.vendor.index');
        }
    }

    public function error_page_redirect(){
        Theme::uses('default')->layout('master.layout');
        return Theme::view('core.dashboard.vendor.redirect');

    }
}
