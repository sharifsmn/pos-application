<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use Modules\Core\Entities\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use Session;
Use Auth;
use Theme;
use Mail;

class UsersController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('view-users')) {
            return abort(401);
        }

        $users = User::where('vendor_id', Auth::user()->id)->orderBy('id', 'asc')->paginate(10);
        Theme::uses('default')->layout('master.layout');
        return Theme::view('core.users.main.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('create-user')) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');
        $locations = Location::all()->pluck('location_name', 'location_id');
        $locations->prepend('Select','');
        Theme::uses('default')->layout('master.layout');
        return Theme::view('core.users.main.create', compact('roles','locations'));
        
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsersRequest $request)
    {
        if (! Gate::allows('create-user')) {
            return abort(401);
        }
        $user = User::create($request->all());
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);
        
        

        // Mail::send('mail.email', ['user' => $request->all()], function ($m) use ($user) {
        //     $m->from('hello@app.com', 'Your Application');

        //     $m->to($user->email, $user->name)->subject('Your Reminder!');
        // });

        
        return redirect()->route('admin.users.index')->with('success','Application deleted successfully');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('update-user')) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');
        $locations = Location::all()->pluck('location_name', 'location_id');
        $locations->prepend('Select','');
        $user = User::findOrFail($id);
        Theme::uses('default')->layout('master.layout');
        return Theme::view('core.users.main.edit',compact('user', 'roles','locations'));
        //return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, $id)
    {
        if (! Gate::allows('update-user')) {
            return abort(401);
        }
        $user = User::findOrFail($id);
        $user->update($request->all());
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);
        //Mail::to($user->email)->send(new UserMail($request->all()));
        // Mail::send('mail.email', ['user' => $request->all()], function ($m) use ($user) {
        //     $m->from('hello@app.com', 'Your Application');

        //     $m->to($user->email, $user->name)->subject('Your Reminder!');
        // });
        
        
        return redirect()->route('admin.users.index')->with('success','Application deleted successfully');
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('delete-user')) {
            return abort(401);
        }
        $user = User::findOrFail($id);
        $user->delete();
        
        return redirect()->route('admin.users.index')->with('success','Application deleted successfully');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('create-user')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = User::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    public function notifications()
    {
        return auth()->user()->unreadNotifications()->limit(5)->get()->toArray();
    }    

}
