<?php

namespace App\Http\Controllers\Admin;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRolesRequest;
use App\Http\Requests\Admin\UpdateRolesRequest;
use Theme;


class RolesController extends Controller
{
    /**
     * Display a listing of Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('view-roles')) {
            return abort(401);
        }
         // foreach (config('coremodules') as $module => $perms){
         //    if(is_array($perms)){
         //        foreach ($perms as $perm => $label){
         //            if(Permission::where('name',$perm)->first()){
                        
         //            }else{
         //              Permission::create(['name' => $perm]);  
         //            }

         //            //
         //        }
         //    }
         // }       
        $roles = Role::all();
        Theme::uses('default')->layout('master.layout');
        return Theme::view('core.users.roles.index',compact('roles'));
    }

    /**
     * Show the form for creating new Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('create-role')) {
            return abort(401);
        }
        $permissions = Permission::get()->pluck('name', 'name');
        Theme::uses('default')->layout('master.layout');
        return Theme::view('core.users.roles.create',compact('permissions'));
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param  \App\Http\Requests\StoreRolesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRolesRequest $request)
    {
        if (! Gate::allows('create-role')) {
            return abort(401);
        }
        $role = Role::create($request->except('permissions'));

        $permissions = $request->input('permissions') ? $request->input('permissions') : [];
        
        $role->syncPermissions($permissions);
    
        Theme::uses('default')->layout('master.layout');
        return redirect()->route('admin.roles.index');
    }


    /**
     * Show the form for editing Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('update-role')) {
            return abort(401);
        }
        $permissions = Permission::get()->pluck('name', 'name');

        $role = Role::findOrFail($id);
        Theme::uses('default')->layout('master.layout');
        return Theme::view('core.users.roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update Role in storage.
     *
     * @param  \App\Http\Requests\UpdateRolesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRolesRequest $request, $id)
    {
        if (! Gate::allows('update-role')) {
            return abort(401);
        }
        $role = Role::findOrFail($id);
        $role->update($request->except('permissions'));
        $permissions = $request->input('permissions') ? $request->input('permissions') : [];
        $role->syncPermissions($permissions);

        return redirect()->route('admin.roles.index');
    }


    /**
     * Remove Role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('delete-role')) {
            return abort(401);
        }
        $role = Role::findOrFail($id);
        $role->revokePermissionTo($role->permissions()->get());
        $role->delete();
        
        return redirect()->route('admin.roles.index');
    }

    /**
     * Delete all selected Role at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('delete-role')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Role::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
