<?php

namespace App\Http\Controllers;

use App\Authorize;
use Illuminate\Http\Request;
use App\Notifications\UserDeviceAuthorize as DeviceAuthorize;
use Auth;
use Theme;
use Illuminate\Support\Facades\Notification;
use App\User;
use Modules\Core\Entities\Floor;
class AuthorizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Authorize  $authorize
     * @return \Illuminate\Http\Response
     */
    public function show(Authorize $authorize)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Authorize  $authorize
     * @return \Illuminate\Http\Response
     */
    public function edit(Authorize $authorize)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authorize  $authorize
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Authorize $authorize)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Authorize  $authorize
     * @return \Illuminate\Http\Response
     */
    public function destroy(Authorize $authorize)
    {
        //
    }

    public function verify(Request $request, Authorize $authorize)
    {
        Theme::uses('default')->layout('authorize.layout');

        $authorize->request_verify_update();

        $user  = Auth::user();
        //$user->notify(new DeviceAuthorize($user));
        Notification::send(User::first(), new DeviceAuthorize($user));

        return redirect()->route('authorize.status');
    }
    
    public function status(Request $request, Authorize $authorize)
    {
        $request_verify = Authorize::request_verify();
        
        if($request_verify->request_verify == 1){
            Theme::uses('default')->layout('authorize.layout');
            return Theme::view('core.authorize.verify_message');
        }else{
            // if(User::role('writer')->get()){
            //     Theme::uses('front')->layout('master.layout');
                
            //     $floors = Floor::where('vendor_id',1)->get();
            //     return Theme::view('frontend.table.index', compact('floors'));

            // }else{
            //     // Theme::uses('default')->layout('master.layout');
            //     // return Theme::view('core.dashboard.vendor.index');
            // }
        }
    }    

    public function authorized(Request $request, Authorize $authorize)
    {
        $authorized_data = Authorize::where('vendor_id',Auth::id())->get();
        
        Theme::uses('default')->layout('master.layout');
        return Theme::view('core.authorize.authorized',compact('authorized_data'));
    }    

    public function authorized_update(Request $request, $id)
    {
        $Authorize = Authorize::findOrFail($id);
        $Authorize->update($request->all());

         $Authorize->update(['authorized' => 1,'request_verify' => 0]);
        return redirect()->route('authorize.authorized');
    }


}
