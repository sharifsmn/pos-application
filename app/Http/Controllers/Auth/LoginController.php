<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use App\User;
use Theme;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authorize')->only('login');    
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        Theme::uses('default')->layout('login.layout');    
        return Theme::view('core.login.index');    
    }

    protected function credentials(\Illuminate\Http\Request $request){

        $credentials = $request->only($this->username(), 'password');
        return array_add($credentials, 'status', '1');
        
     }
    
}
