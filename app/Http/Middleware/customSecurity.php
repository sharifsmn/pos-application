<?php

namespace App\Http\Middleware;

use Closure;
use Modules\Core\Entities\Iptables;

class customSecurity
{
    
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $whitelist = Iptables::whitelist_ips()->pluck('ip_address')->toArray();
        //$blacklist = Iptables::blacklist_ips()->get();
        $config = config('core');
        if (is_array($whitelist)) {
            if(in_array($request->getClientIp(), $whitelist)){
                
                return $next($request);  
            }else{
                if ( $config['redirect_to'] ) {
                    return redirect()->to($config['redirect_to']);
                }else{
                    \Log::warning("Unauthorized access, IP address was => ".request()->ip);

                    return response()->json(['Unauthorized!'],$config['response_status']);
                }                  
            }
        }

        return $next($request);   
    }

    
}
