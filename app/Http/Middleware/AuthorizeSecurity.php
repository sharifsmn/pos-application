<?php

namespace App\Http\Middleware;

use Closure;
use App\Authorize;
use Auth;
use Theme;
Theme::uses('default')->layout('authorize.layout');
class AuthorizeSecurity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (Authorize::inactive() && auth()->check()) {
            Authorize::make();
            

            if(Authorize::checkVendor_id()->vendor_id !== NULL){
                Authorize::increment('attempt');
                $request_verify = Authorize::request_verify();

                if($request_verify->request_verify === 1){

                    return Theme::view('core.authorize.message');
                }

                return Theme::view('core.authorize.index');            

            }
        }

        return $next($request);
    }
}
