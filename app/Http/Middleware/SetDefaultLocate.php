<?php

namespace App\Http\Middleware;

use Closure;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\App;
use Modules\Core\Entities\Languages;

class SetDefaultLocate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        // $default_language = \Modules\Core\Entities\Settings::where('option_key','default_language')->first();
        // $language = \Modules\Core\Entities\Languages::where('id', $default_language->option_value)->first();
        // if(LaravelLocalization::getCurrentLocale() == $language->code){
        //     app()->setLocale($language->code);
        //     LaravelLocalization::setLocale($language->code);
        // }else{
        //     app()->setLocale(LaravelLocalization::getCurrentLocale());
        //     LaravelLocalization::setLocale(LaravelLocalization::getCurrentLocale());
            
        // }
        return $next($request);
    }
}
