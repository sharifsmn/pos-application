<?php

namespace App\AuthorizeLib;
/**
 * File: AuthorizeDevice.php
 * Author: Faysal Mahamud
 *
 * Class AuthorizeDevice
 * @package App
 * 
 */
use App\AuthorizeLib\Browser;
use App\AuthorizeLib\UUID;

/**
 * Class AuthorizeDevice
 * @package App\Mail
 */
class AuthorizeDevice
{

    /**
     * @var mixed
     */
    protected $authorize;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @param $authorize
     *  @return void
     */
    public function __construct($authorize, $user)
    {
        $this->authorize = $authorize;
        $this->browser = new Browser;
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function setBrowser()
    {
        $this->authorize->browser = $this->browser->getBrowser();

        return $this;
    }

    /**
     * @return mixed
     */
    public function setToken()
    {

        $this->authorize->token = new UUID();
        $this->authorize->user_id = $this->user->id;
        $this->authorize->vendor_id = $this->user->vendor_id;

        return $this;
    }


    /**
     * @return mixed
     */
    public function setPlatform()
    {
        $this->authorize->os = $this->browser->getPlatform();

        return $this;
    }

    public function saveAuthorize()
    {
        $this->authorize->save();
    }

   

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this
            ->setBrowser()
            ->setToken()
            ->setPlatform()
            ->saveAuthorize();

        return $this;
    }
}
