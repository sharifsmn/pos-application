<?php

namespace App\AuthorizeLib;

/**
 * File: UUID.php
 * Author: Faysal Mahamud
 *
 * Class UUID
 * @package App
 * 
 */
 
class UUID {

    /**
     * @var
     */
    public $prefix;

    /**
     * @var
     */
    public $entropy;

    /**
     * @param string $prefix
     * @param bool $entropy
     */
    public function __construct($prefix = '', $entropy = false)
    {
        $this->uuid = uniqid($prefix, $entropy);
    }

    /**
     * Limit the UUID by a number of characters
     * 
     * @param $length
     * @param int $start
     * @return $this
     */
    public function limit($length, $start = 0)
    {
        $this->uuid = substr($this->uuid, $start, $length);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->uuid;
    }
    
    public function guid($trim = true){
    
      mt_srand((double) microtime() * 10000);
      $charid = strtolower(md5(uniqid(rand(), true)));
      $hyphen = chr(45); // "-"
      $lbrace = $trim ? "" : chr(123); // "{"
      $rbrace = $trim ? "" : chr(125); // "}"
      $guidv4 = $lbrace .
      substr($charid, 0, 8) . $hyphen .
      substr($charid, 8, 4) . $hyphen .
      substr($charid, 12, 4) . $hyphen .
      substr($charid, 16, 4) . $hyphen .
      substr($charid, 20, 12) . $rbrace;
      
      return $guidv4;
    }
} 
