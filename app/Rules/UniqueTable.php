<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Restaurant\Entities\Table;

class UniqueTable implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $exists;
     
    public function __construct($check)
    {
        $this->exists = $check;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //dd($this->exists['location_id']);
        $res = Table::where('floor_id',$this->exists['floor_id'])->where('table_name', $value)->count();
        if($res > 0){
          return false;
        }else{
          return true;
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be Unique for the location.';
    }
}
