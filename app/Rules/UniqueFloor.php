<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Core\Entities\Floor;

class UniqueFloor implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $exists;
     
    public function __construct($check)
    {
        $this->exists = $check;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //dd($this->exists['location_id']);
        $res = Floor::where('location_id',$this->exists['location_id'])->where('floor_name', $value)->count();
        if($res > 0){
          return false;
        }else{
          return true;
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be Unique for the location.';
    }
}
