<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Hash;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
*/
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $fillable = ['vendor_id', 'location_id', 'name', 'email', 'password', 'remember_token','status'];
    
    protected $casts = [
     'status' => 'boolean',
    ];

    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }
    
    
    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }
    
    public function findForPassport($identifier) {
        return User::orWhere('email', $identifier)->where('status', 1)->first();
    }    
    
    public function userProfile()
    {
        return $this->hasOne('Modules\Core\Entities\Profiles');
    }   
    
}
