<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\AuthorizeLib\Browser;
use App\AuthorizeLib\UUID;

class Authorize extends Model
{
    //
    protected $fillable = [
        'user_id','vendor_id', 'authorized', 'request_verify', 'ip_address', 'browser', 'os', 'attempt', 'authorized_at',
    ];

    public static function make(){
 		$id = Auth::id();
 		$browser = new Browser();
        return self::firstOrCreate([
            'ip_address' => Request::ip(),
            'authorized' => false,
            'user_id' => $id,
            'vendor_id' => Auth::user()->vendor_id,
            'browser'=> $browser->getBrowser(),
            'os'=> $browser->getPlatform(),
        ]);
    }

    public static function checkVendor_id(){
        return Authorize::select('vendor_id')->where('user_id',Auth::id())->first();
    }


    public function resetAttempt()
    {
        $this->update(['attempt' => 0]);

        return $this;
    }

    public function noAttempt()
    {
        return $this->request_verify < 1;
    }

    //
    public static function active()
    {
        return with(new self)
            ->where('ip_address', Request::ip())
            ->where('authorized', true)
            ->first();
    }    

	//
    public static function inactive()
    {
        $query = self::active();
       
        return $query ? null : true;
    }

    public static function request_verify(){
    	return Authorize::select('request_verify')->where('user_id',Auth::id())->first();
    }

    public function request_verify_update()
    {
        $this->where('user_id',Auth::id())->update(['request_verify' => 1]);

        return $this;
    }

}
