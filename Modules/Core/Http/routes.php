<?php

$languages = Modules\Core\Entities\Languages::select('code')->where('status', 1)->pluck('code')->toArray();
$locale = Request::segment(1);

if(in_array($locale, $languages)){
    app()->setLocale($locale);
}else{
    $default_language = \Modules\Core\Entities\Settings::where('option_key','default_language')->first();
    $language = \Modules\Core\Entities\Languages::where('id', $default_language->option_value)->first();
    app()->setLocale($language->code);

    $locale = $language->code;
}

Route::group(['prefix' => $locale], function()
{        
   Route::group(['middleware' => ['web','auth'], 'prefix' => 'core', 'namespace' => 'Modules\Core\Http\Controllers'], function()
    {
                
        Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'CoreController@dashboard']);

    });
}); 
Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    Route::group(['middleware' => ['web','auth'], 'prefix' => 'core', 'namespace' => 'Modules\Core\Http\Controllers'], function()
    {
        // Dashboard
        Route::get('/', 'CoreController@index');

        // Pos Sale Routes
        Route::get('/pos-sale/list', 'CoreController@pos_sales_list');
        // Order Sale Routes
        Route::get('/order-sale/list', 'CoreController@order_sales_list');
        // Reporting Routes
        Route::get('/sale-report/list', 'CoreController@sales_report_list');
        Route::get('/product-sale/list', 'CoreController@product_sale_list');
        Route::get('/product-graphs/list', 'CoreController@product_graphs_list');
        Route::get('/sales-sold/list', 'CoreController@sales_sold_list');
        // Category Routes
        Route::get('/category/list', 'CoreController@category_list');
        Route::get('/category/add', 'CoreController@category_add');
        // Product Route
        Route::get('/product/add', 'CoreController@product_add');
        Route::get('/product/list', 'CoreController@product_list'); 
        // User Routes
        Route::get('/user/list', 'CoreController@user_list');      
        Route::get('/user/add', 'CoreController@user_add');
        // Roll Routes
        Route::get('/roll/list', 'CoreController@roll_list');
        Route::get('/roll/add', 'CoreController@roll_add');
        // Permissions Routes
        Route::get('/permissions', 'CoreController@permission'); 
        // Menus  Routes
        Route::get('/menu/list', 'CoreController@menu_list'); 
        Route::get('/menu/add', 'CoreController@menu_add'); 
        // Super admin  Routes
        Route::get('/sadmin/list', 'CoreController@sadmin_list'); 
        Route::get('/sadmin/add', 'CoreController@sadmin_add'); 

        // Profile
        Route::get('/front/profile', 'ProfileController@forntend_profile');    
        Route::resource('profile', 'ProfileController');    
        
    });

    Route::group(['middleware' => ['web','auth'], 'prefix' => 'configuration', 'namespace' => 'Modules\Core\Http\Controllers'], function()
    {
        Route::post('/settings/default_update', 'Configuration\SettingsController@default_update')->name('supdate');

        Route::resource('settings', 'Configuration\SettingsController');
        
        Route::resource('/activities', 'Configuration\ActivitiesController');

        Route::resource('/countries', 'Configuration\CountriesController');

        Route::resource('currencies', 'Configuration\CurrenciesController');

        Route::post('/languages/default_update', 'Configuration\LanguagesController@default_update')->name('languages.mdefault');
        
        Route::resource('languages', 'Configuration\LanguagesController');

        Route::resource('translations', 'Configuration\TranslationsController');

        Route::resource('/order_status', 'Configuration\OrderStatusController');

        Route::get('/table_status', 'Configuration\OrderStatusController@table_status');


        Route::resource('/emailaccounts', 'Configuration\EmailAccountsController');

        Route::resource('/stores', 'Configuration\StoresController');

        //Route::resource('/taxes/settings', 'Configuration\TaxesController');

        Route::post('/taxes/default_update', 'Configuration\TaxProvidersController@default_update')->name('taxes.mdefault');

        Route::resource('taxes', 'Configuration\TaxProvidersController');
        Route::resource('/taxes/categories', 'Configuration\TaxCategoriesController');

        Route::resource('/payments', 'Configuration\PaymentController');
        Route::resource('/shipping', 'Configuration\ShippingController');
        
        Route::get('/blacklisted', 'WhiteListedController@BlackListed');
        Route::resource('/whitelisted', 'WhiteListedController');

    });


    Route::group(['middleware' => ['web','auth'], 'prefix' => 'sadmin', 'namespace' => 'Modules\Core\Http\Controllers'], function()
    {

        Route::get('users', array('as' => 'users.index', 'uses' => 'SadminController@client_list'));
        Route::get('users/add', array('as' => 'users.create', 'uses' => 'SadminController@client_create'));
        Route::post('users/store', array('as' => 'users.store', 'uses' => 'SadminController@client_store'));
        Route::get('users/edit/{id}', array('as' => 'users.edit', 'uses' => 'SadminController@client_edit'));
        Route::put('users/update/{id}', array('as' => 'users.update', 'uses' => 'SadminController@client_update'));
        Route::delete('users/delete/{id}', array('as' => 'users.destroy', 'uses' => 'SadminController@client_delete'));
        // Route::get('users/{slug}', array('as' => 'users.show', 'uses' => 'SadminController@show'));

    });

    Route::group(['middleware' => ['web','auth'], 'prefix' => 'frontend', 'namespace' => 'Modules\Core\Http\Controllers'], function()
    {

        Route::get('/reservation/get/', [
                    'as' => 'reservation_preview', 'uses' => 'FrontendController@reservation'
                    ]);  

        Route::get('/login', 'FrontendController@login');   
        Route::get('/table', 'FrontendController@table');   
        Route::get('/table', [
                    'as' => 'table', 'uses' => 'FrontendController@table'
                    ]);   
        Route::get('/order/{id}', 'FrontendController@order');
        Route::get('/order_preview/{order_id}', [
                    'as' => 'order_preview', 'uses' => 'FrontendController@order_preview'
                    ]);
        Route::get('/order_edit/{order_id}', [
                    'as' => 'order_edit', 'uses' => 'FrontendController@order_edit'
                    ]);
        Route::get('/order_confirm/{order_id}', [
                    'as' => 'order_confirm', 'uses' => 'FrontendController@order_confirm'
                    ]);
        Route::get('/order_invoice/{order_id}', [
                    'as' => 'order_invoice', 'uses' => 'FrontendController@order_invoice'
                    ]);
        
        Route::get('/order_print/{order_id}', [
                    'as' => 'order_print', 'uses' => 'FrontendController@order_print'
                    ]);

        Route::get('/direct_recieved/{order_id}', ['as' => 'direct_recieved', 'uses' => 'FrontendController@direct_recieved']);

        Route::get('/payment_form/{order_id}', ['as' => 'payment_form', 'uses' => 'FrontendController@payment_form']);

        Route::post('/payment_submit/{order_id}', ['as' => 'payment_submit', 'uses' => 'FrontendController@payment_submit']);


        Route::post('/menu_search','FrontendController@search_menu');
        Route::post('/search_cat','FrontendController@search_cat');
        Route::post('/customer_save','FrontendController@customer_save');
        Route::post('/customer_ist','FrontendController@customer_list');
        Route::post('/payment_save','FrontendController@payment_save');
        Route::post('/order_save','FrontendController@order_save');
        Route::post('/order_update','FrontendController@order_update');
        // Route::get('/reorder/', [
        //             'as' => 'reorder', 'uses' => 'FrontendController@reorder'
        //             ]);
        Route::post('/reorder/{order_id}', [
                    'as' => 'reorder', 'uses' => 'FrontendController@reorder'
                    ]);
        Route::post('/waittime_save','FrontendController@waittime_save');
        Route::post('/waittime_list','FrontendController@waittime_list');
        Route::post('/takeout_complete','FrontendController@takeout_complete');

    });


    Route::group(['middleware' => ['web','auth'], 'prefix' => 'product', 'namespace' => 'Modules\Core\Http\Controllers'], function()
    {

        // Products
        
        Route::resource('categories', 'CategoriesController');
        Route::resource('menus', 'MenusController'); 
        Route::resource('/menu_options', 'MenuOptionsController'); 
        Route::resource('/tags', 'TagsController');
       
    });
    Route::group(['middleware' => ['web','auth'], 'prefix' => 'filemanager', 'namespace' => 'Modules\Core\Http\Controllers'], function()
    {

        Route::get('/', 'FilemanagerController@index');
       
    });
    Route::group(['middleware' => ['web','auth']], function() {
        // Mail Box
        Route::get('/mailbox/compose', 'Modules\Core\Http\Controllers\MailboxController@mail_compose');
        Route::resource('mailbox', 'Modules\Core\Http\Controllers\MailboxController');
    });
    
   Route::group(['middleware' => ['web','auth'], 'prefix' => 'cashier', 'namespace' => 'Modules\Core\Http\Controllers'], function()
    {    
        Route::get('/', ['as' => 'cashier', 'uses' => 'CashierController@index']);
        Route::get('/get/{order_id}', ['as' => 'cashier', 'uses' => 'CashierController@customer_info']);
        Route::post('/', ['as' => 'cashier', 'uses' => 'CashierController@index']);
        Route::resource('payments', 'CashierController');     
    });

});