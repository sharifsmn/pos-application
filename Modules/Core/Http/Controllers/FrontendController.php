<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Floor;
use Modules\Core\Entities\Menu;
use Modules\Restaurant\Entities\Table;
use Modules\Core\Entities\Categories;
use Modules\Core\Entities\Customers;
use Modules\Core\Entities\Orders;
use Modules\Core\Entities\Order_menu;
use Modules\Core\Entities\Order_totals;
use Modules\Core\Entities\Assigned_table;
use Modules\Core\Entities\Payments;
use Modules\Restaurant\Entities\Waitingline;
use Modules\Core\Entities\sale_history;
use Modules\Core\Entities\Reservations;
use App\AuthorizeLib\Browser;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use App\Notifications\SiteNotification as SiteNotification;
use Illuminate\Support\Facades\Notification;

use Theme;
Theme::uses('front')->layout('master.layout');

class FrontendController extends Controller
{

    public function login()
    {
        return Theme::view('frontend.login.login');
    }
    public function table()
    {
        $user = \Auth::user();
        if($user->hasRole('waiter')){       
            $floors = Floor::where('vendor_id',1)->where('location_id', \Auth::user()->location_id )->get();
            return Theme::view('frontend.table.index', compact('floors'));
        }else{
            return Theme::view('frontend.table.locations');            
        }
    }
    public function order()
    {   
        return Theme::view('frontend.order.index');
    }

    public function search_menu(Request $request){
        if($request->category){
            $menus = Menu::where('menu_name','LIKE','%'.$request->search."%")
                ->where('menu_category_id', $request->category)
                ->where('status', 1)
                ->get();
        }else{
            $menus = Menu::where('menu_name','LIKE','%'.$request->search."%")
                ->where('status', 1)
                ->get();

        }    
        $theme = Theme::uses('front')->layout('blank.layout');
        if($menus->count() > 0){
            $html = Theme::scope('frontend.order.search', compact('menus'))->content();
        }else{
            $html = 'no item found';
        }
        return array('success' => $html);
    }

    public function search_cat(Request $request){
        if($request->category){
            $menus = Menu::where('menu_category_id', $request->category)
                ->where('status', 1)
                ->get();
        }else{
            $menus = Menu::all();
        }            
        $theme = Theme::uses('front')->layout('blank.layout');
        if($menus->count() > 0){
            $html = Theme::scope('frontend.order.search', compact('menus'))->content();
        }else{
            $html = 'no item found';
        }
        return array('success' => $html);
    }

    // main order save funcationality.
    public function order_save(Request $request){
        $browser = new Browser();
        $order_data = array(
            'vendor_id' => 1,
            'table_id' => $request->table_id,
            'first_name' => $request->c_name, 
            'last_name' => $request->l_name,
            'email' => $request->email,
            'telephone' =>  $request->phone,
            'total_items' =>  $request->cart_total_item_count,
            'status_id' =>  $request->order_status,
            'ip_address' =>  $request->ip(),
            'user_agent' =>  $browser->getBrowser(),
            'assignee_id' =>  \Auth::user()->id,
            'invoice_no' =>  0,
            'payment' => $request->type_pay,
            'order_total' => $request->cart_submitted_total,
            'cart' => serialize($request->all()),
        );
        $order = Orders::create($order_data);
         
        $sale_history = sale_history::create([
                'vendor_id' => 1,
                'order_id' => $order->order_id,
                'staff_id' => \Auth::user()->id,
                'status_id' => $request->order_status,
                'notify' => 1,
                'status_type' => 'order',
            ]);
        
        if($request->menu_item){
            $category_id = [];
            foreach($request->menu_item as $key => $val):
                $menu_info = Menu::where('menu_id', $key)->first();
                $category_id[] = $menu_info->menu_category_id;
                $order_menu = Order_menu::create([
                    'vendor_id' => 1 ,
                    'order_id' => $order->order_id,
                    'table_id'=> $request->table_id, 
                    'menu_id' => $key,
                    'name' => $menu_info->menu_name,
                    'quantity' => $val['item_number'],
                    'orginal_price' => $val['original_price'],
                    'price' => $val['cart_item_price'],
                    'subtotal'=> $val['cart_item_total'],
                    'notes'=> isset($val['notes'])? $val['notes']: '',                        
                    'tax' => $val['cart_item_tax'],
                    
                ]);
                
            endforeach;   
        }
        
        $cat_id = implode(',',$category_id);
        
        Orders::where('order_id',$order->order_id)->update([
            'category_id' => $cat_id,
        ]);
        $order_totals = Order_totals::create([
          'vendor_id' => 1,
          'order_id' => $order->order_id,
          'table_id'=> $request->table_id,
          'subtotal' => $request->cart_order_subtotal,
          'discount' => $request->cart_discount,
          'global_tax' => $request->cart_global_tax,
          'submitted_total' => $request->cart_submitted_total,
          'total_item_count' => $request->cart_total_item_count,
        ]);
        $assigned_table = Assigned_table::create([
            'vendor_id' => 1,
            'order_id' => $order->order_id,
            'table_id'=> $request->table_id,             
        ]);        

        $payments = Payments::create([
            'vendor_id' => 1,
            'table_id'=> $request->table_id,
            'order_id' => $order->order_id,
            'serialized'=> serialize($request->payment_method),             
        ]);
        
        
        $waiting_line = Waitingline::where('id', $request->submitted_waittime_id)->update([
            'order_id' => $order->order_id,
        ]);

        $order_status = \Modules\Core\Entities\Order_status::where('status_id',$request->order_status)->first();


        $users = \App\User::permission('view-live-kitchen')->where('status',1)->get();
        $current_user = \Auth::user();
        
        $message = "Order $order_status->status_name Successfully" . ' Order Id: '. $order->order_id;
        
        foreach($users as $user){
            
            Notification::send($user, new SiteNotification($current_user,$message));
        }

        return redirect()->route('order_preview', ['order_id' => $order->order_id]);
    }

    public function order_update(Request $request){
        $browser = new Browser();
        $order_data = array(
            'vendor_id' => 1,
            'table_id' => $request->table_id,
            'first_name' => $request->c_name, 
            'last_name' => $request->l_name,
            'email' => $request->email,
            'telephone' =>  $request->phone,
            'total_items' =>  $request->cart_total_item_count,
            'status_id' =>  $request->order_status,
            'ip_address' =>  $request->ip(),
            'user_agent' =>  $browser->getBrowser(),
            'assignee_id' =>  \Auth::user()->id,
            'invoice_no' =>  0,
            'payment' => $request->type_pay,
            'order_total' => $request->cart_submitted_total,
            'cart' => serialize($request->all()),
        );

        $order = Orders::where('order_id', $request->order_id)->update($order_data);

        $delete_history = sale_history::where('order_id', $request->order_id)->delete();
        
        $order_menu = Order_menu::where('order_id', $request->order_id)->delete();
        $order_totals = Order_totals::where('order_id', $request->order_id)->delete();
        $assigned_table = Assigned_table::where('order_id', $request->order_id)->delete();
        
         
        $sale_history = sale_history::create([
                'vendor_id' => 1,
                'order_id' => $request->order_id,
                'staff_id' => \Auth::user()->id,
                'status_id' => $request->order_status,
                'notify' => 1,
                'status_type' => 'order',
            ]);
        
        if($request->menu_item){
            $category_id = [];
            foreach($request->menu_item as $key => $val):
                $menu_info = Menu::where('menu_id', $key)->first();
                $category_id[] = $menu_info->menu_category_id;
                $order_menu = Order_menu::create([
                    'vendor_id' => 1 ,
                    'order_id' => $request->order_id,
                    'table_id'=> $request->table_id, 
                    'menu_id' => $key,
                    'name' => $menu_info->menu_name,
                    'quantity' => $val['item_number'],
                    'orginal_price' => $val['original_price'],
                    'price' => $val['cart_item_price'],
                    'subtotal'=> $val['cart_item_total'],
                    'notes'=> isset($val['notes'])? $val['notes']: '',                        
                    'tax' => $val['cart_item_tax'],
                    
                ]);
                
            endforeach;   
        }
        
        $cat_id = implode(',',$category_id);
        
        Orders::where('order_id',$request->order_id)->update([
            'category_id' => $cat_id,
        ]);
        $order_totals = Order_totals::create([
          'vendor_id' => 1,
          'order_id' => $request->order_id,
          'table_id'=> $request->table_id,
          'subtotal' => $request->cart_order_subtotal,
          'discount' => $request->cart_discount,
          'global_tax' => $request->cart_global_tax,
          'submitted_total' => $request->cart_submitted_total,
          'total_item_count' => $request->cart_total_item_count,
        ]);
        $assigned_table = Assigned_table::create([
            'vendor_id' => 1,
            'order_id' => $request->order_id,
            'table_id'=> $request->table_id,             
        ]);        

        $payments = Payments::create([
            'vendor_id' => 1,
            'table_id'=> $request->table_id,
            'order_id' => $request->order_id,
            'serialized'=> serialize($request->payment_method),             
        ]);
        

        $order_status = \Modules\Core\Entities\Order_status::where('status_id',$request->order_status)->first();


        $users = \App\User::permission('view-live-kitchen')->where('status',1)->get();
        $current_user = \Auth::user();
        
        $message = "Order $order_status->status_name Successfully" . ' Order Id: '. $request->order_id;
        
        foreach($users as $user){
            
            Notification::send($user, new SiteNotification($current_user,$message));
        }

        return redirect()->route('order_preview', ['order_id' => $request->order_id]);
    }

    public function reorder(Request $request){
            
        $order = Orders::where('order_id', $request->order_id)->first();
        $newOrder = $order->replicate();
        $newOrder->status_id = 2;
        $newOrder->save();

        $order_totals = Order_totals::where('order_id', $request->order_id)->first();
        $new_order_totals = $order_totals->replicate();
        $new_order_totals->order_id = $newOrder->order_id;
        $new_order_totals->save();
        

        $assigned_tables = Assigned_table::where('order_id', $request->order_id)->first();
        $assigned_table = $assigned_tables->replicate();
        $assigned_table->order_id = $newOrder->order_id;
        $assigned_table->status = 1;
        $assigned_table->save();

        $payments = Payments::where('order_id', $request->order_id)->get();
        foreach($payments as $key => $payment){
            $newpayment = $payment->replicate();
            $newpayment->order_id = $newOrder->order_id;
            $newpayment->save();
        }


        $current_order_menus = Order_menu::where('order_id', $request->order_id)->get();
        foreach($current_order_menus as $key => $current_order_menu){
            $new_order_menu = $current_order_menu->replicate();
            $new_order_menu->order_id = $newOrder->order_id;
            $new_order_menu->save();
        }

        $sale_history = sale_history::create([
                'vendor_id' => 1,
                'order_id' => $newOrder->order_id,
                'staff_id' => \Auth::user()->id,
                'status_id' => $newOrder->status_id,
                'notify' => 1,
                'status_type' => 'reorder',
        ]);

        $order_status = \Modules\Core\Entities\Order_status::where('status_id',$newOrder->status_id)->first();
        $users = \App\User::permission('view-live-kitchen')->where('status',1)->get();
        $current_user = \Auth::user();
        
        $message = "Order $order_status->status_name Successfully" . ' Order Id: '. $newOrder->order_id;
        
        foreach($users as $user){
            
            Notification::send($user, new SiteNotification($current_user,$message));
        }

        return redirect()->route('order_invoice', ['order_id' => $newOrder->order_id]);        
    }

    public function order_preview(Request $request){

        return Theme::view('frontend.order.preview');
    }

    public function order_edit(Request $request){

        return Theme::view('frontend.order.order_edit');
    }     
    public function order_confirm(Request $request){
        $order = Orders::where('order_id', $request->order_id)->update([
            'status_id' => 2,
        ]);

        $sale_history = sale_history::create([
                'vendor_id' => 1,
                'order_id' => $request->order_id,
                'staff_id' => \Auth::user()->id,
                'status_id' => 2,
                'notify' => 1,
                'status_type' => 'order',
            ]);

        $order_status = \Modules\Core\Entities\Order_status::where('status_id',2)->first();


        $users = \App\User::permission('view-live-kitchen')->where('status',1)->get();
        $current_user = \Auth::user();
        
        $message = "Order $order_status->status_name Successfully" . ' Order Id: '. $request->order_id;
        
        foreach($users as $user){
            
            Notification::send($user, new SiteNotification($current_user,$message));
        }

        return redirect()->route('order_invoice', ['order_id' => $request->order_id])->with(['success' => 'The order confirmed']);
    } 

    public function order_invoice(Request $request){
        Theme::uses('front')->layout('inovice.layout');
        return Theme::view('frontend.order.invoice');
    }


    public function waittime_list(Request $request){

        $waiting_time = Waitingline::where('id', $request->waiting_id)->update([
            'status' => 0,
            'takeout' => 1,
        ]);
        if($waiting_time){
            $waiting = Waitingline::where('status', 1)->get();
            $html = '';
            foreach($waiting as $key => $value){
                            $time = \Carbon\Carbon::parse($value->waiting)->format('H:i:s');
                            $notify = ($value->notify == 1) ? 'Yes' : 'No';
                            $table = Table::where('table_id',$value->assigned_table)->first()->table_name;
                            $html .= "<tr>
                                <td>$value->capacity</td>
                                <td>$value->name</td>
                                <td>
                                 $time
                                </td>
                                <td>$notify</td>
                                <td>$table</td>
                                <td><input class=\"waiting_id\" type=\"hidden\" value=\"$value->id\"><input class=\"waiting_complete\" type=\"checkbox\" name=\"done\"></td>
                            </tr>";
            }
                return array('success' => $html);
        }    
    }

    public function takeout_complete(Request $request){
        $html = '';
        $takeout = Waitingline::findOrFail($request->waiting_id);
        $takeout->delete();

        
            $waiting = Waitingline::where('takeout', 1)->get();
            $html = '';
            foreach($waiting as $key => $value){
                            $time = \Carbon\Carbon::parse($value->waiting)->format('H:i:s');
                            $notify = ($value->notify == 1) ? 'Yes' : 'No';
                            $table = Table::where('table_id',$value->assigned_table)->first()->table_name;
                            $html .= "<tr>
                                <td>$value->capacity</td>
                                <td>$value->name</td>
                                <td>
                                 $time
                                </td>
                                <td>$notify</td>
                                <td>$table</td>
                                <td><input class=\"takeout_id\" type=\"hidden\" value=\"$value->id\"><input class=\"takeout_complete\" type=\"checkbox\" name=\"done\"></td>
                            </tr>";
            }
            
            return array('success' => $html);
        
    }

    public function reservation(Request $request){
        $customers = Customers::create([
            'vendor_id' => 1,
            'first_name' => $request->all()['c_name'],    
            'last_name' => $request->all()['c_name'],    
            'email' => $request->all()['c_email'],    
            'telephone' => $request->all()['c_phone'],    
            'ip_address' => $request->ip(),    
            'status' => 1,    

        ]);

        if($customers){
            $reservation = Reservations::create([
                    'vendor_id' => 1,
                    'location_id' => $request->all()['location_id'],
                    'floor_id' => $request->all()['floor_id'],
                    'table_id' => $request->all()['table_id'],
                    'customer_id' => $customers->id,
                    'name' => $request->all()['c_name'],
                    'email' => $request->all()['c_email'],
                    'telephone' => $request->all()['c_phone'],
                    'reserve_time' => date('h:i:a', strtotime($request->all()['r_time'])),
                    'reserve_date' => date('Y-m-d', strtotime($request->all()['r_date'])),
                    'status' => 7,
            ]);
        }
        return array('success' => 'ok');


    }

    //check existing customer

    public function customer_list(Request $request){
       $customer_list = Customers::where('id', $request->customer_id)->first();
       
       $html = array(
                'firstname' => $customer_list->first_name,
                'last_name' => $customer_list->last_name,
                'email' => $customer_list->email,
                'telephone' => $customer_list->telephone,
       );
       $current_time = \Carbon\Carbon::now()->toDateTimeString();

       $waitingline = Waitingline::create([
                'vendor_id' => 1,
                'capacity'=> Table::where('table_id',$request->table_id)->first()->max_capacity,
                'name' => $customer_list->first_name . '' . $customer_list->last_name,
                'waiting' => $current_time,
                'assigned_table' => $request->table_id,
                'notify'=> 0,
                'status' => 0,
                'takeout' => 1,
       ]);

       $html['waittime_id'] = $waitingline->id;
       $html['customer_id'] = $request->customer_id; 
       return array('success' => $html); 
    }


    public function customer_save(Request $request){
        
        $c_name = $request->c_name;
        $l_name = $request->l_name;
        $phone =    $request->phone;
        $firstname = $request->email;
        $saved = $request->saved;
        
        if($saved){
            $customers = Customers::create([
                'vendor_id' => 1,
                'first_name' => $c_name,    
                'last_name' => $l_name,    
                'email' => $l_name,    
                'telephone' => $phone,    
                'ip_address' => $request->ip(),    
                'status' => 1,    

            ]);

            if($customers){
              $html['customer_id'] = $customers->customer_id; 
            }
            else{
                $html['customer_id'] = ''; 
            }

        }    

       $waitingline = Waitingline::create([
                'vendor_id' => 1,
                'capacity'=> Table::where('table_id',$request->table_id)->first()->max_capacity,
                'name' => $customers->first_name . '' . $customers->last_name,
                'waiting' => \Carbon\Carbon::now()->toDateTimeString(),
                'assigned_table' => $request->table_id,
                'notify'=> 0,
                'status' => 0,
                'takeout' => 1,
       ]);
            // $customers->id
           $html = array(
                    'firstname' => $request->c_name,
                    'last_name' => $request->l_name,
                    'email' => $request->email,
                    'telephone' => $request->phone,
           );
           $html['waittime_id'] = $waitingline->id;
        return array('success' => $html);    

    }
    //customer wait time
    public function waittime_save(Request $request){
        $waiting_time_update = [
            'qouted' => $request->qouted,
            'notes' => $request->qouted,

        ];

        if($request->waittimesatus == 1){
            $waiting_time_update['notify'] = 1;
            $waiting_time_update['takeout'] = 0;
            $waiting_time_update['status'] = 1;

        }

        Waitingline::where('id',$request->waittime_id)->update($waiting_time_update);
       return array('waittime_id' => $request->waittime_id ); 
    }    

    public function order_print(){
        

        
        try {
            $connector = new NetworkPrintConnector('192.168.0.249', '9100');
            
            /* Print a "Hello world" receipt" */
            $printer = new Printer($connector);
            
            $printer -> text("Hello World!\n");
            $printer -> cut();
            
            /* Close printer */
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }
    public function direct_recieved(Request $request){

        $order_data = array(
            'status_id' =>  6,
            'payment' => 'normal_pay',
        );        
        $order = Orders::where('order_id', $request->order_id)->update($order_data);
        $order_list = Orders::where('order_id', $request->order_id)->first();

        $data = array(
            'payable_amount' => $order_list->order_total,
            'method_type' => 'cash' 
        );
        $payments_data = array(
            'serialized'=> serialize($data),
        );        
        
        $order_list = Payments::where('order_id', $request->order_id)->first();
        if($order_list){
            $payments = Payments::where('order_id', $request->order_id)->update($payments_data);
        }else{
            $payments = Payments::create([
                'vendor_id' => 1,
                'table_id'=> $request->table_id,
                'order_id' => $order->order_id,
                'serialized'=> $payments_data,             
            ]);
        } 
        
        $sale_history = sale_history::create([
                'vendor_id' => 1,
                'order_id' => $request->order_id,
                'staff_id' => \Auth::user()->id,
                'status_id' => 6,
                'notify' => 1,
                'status_type' => 'order',
        ]);

        return redirect()->route('order_invoice',['order_id' => $request->order_id ])->with(['success' => 'The order payment cleared, Print The Invoices']);        
    }    

    public function payment_form(){
        
        return Theme::view('frontend.order.spilit_pay');        
    }
    public function payment_submit(Request $request){
        $order_data = array(
            'status_id' =>  6,
            'payment' => 'split_pay',
        );

        $order = Orders::where('order_id', $request->order_id)->update($order_data);
        
        $payments_data = array(
            'serialized'=> serialize($request->payment_method),
        );        
              
        
        $order_list = Payments::where('order_id', $request->order_id)->first();
        if($order_list){
            $payments = Payments::where('order_id', $request->order_id)->update($payments_data);
        }else{
            $payments = Payments::create([
                'vendor_id' => 1,
                'table_id'=> $request->table_id,
                'order_id' => $order->order_id,
                'serialized'=> $payments_data,             
            ]);
        }

        $sale_history = sale_history::create([
                'vendor_id' => 1,
                'order_id' => $request->order_id,
                'staff_id' => \Auth::user()->id,
                'status_id' => 6,
                'notify' => 1,
                'status_type' => 'order',
        ]);
        
        return redirect()->route('order_invoice',['order_id' => $request->order_id ])->with(['success' => 'The order payment cleared, Print The Invoices']);         
        
    }
}
