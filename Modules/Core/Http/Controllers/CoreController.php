<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;
use Auth;
use Modules\Core\Entities\Floor;

use Theme;
Theme::uses('default')->layout('master.layout');

class CoreController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
    
        return Theme::view('core.dashboard.vendor.index');
    }

    /*
      Later Replace Orginal Module Place
    */
    
    public function dashboard()
    {
        $user = Auth::user();
        
        if($user->hasRole('waiter')){
            Theme::uses('front')->layout('master.layout');
            $floors = Floor::where('vendor_id',1)->where('location_id', \Auth::user()->location_id )->get();
            return Theme::view('frontend.table.index', compact('floors'));
        }else{
            Theme::uses('default')->layout('graphs.dashboard-layout');
            return Theme::view('core.dashboard.vendor.index');            
            // Theme::uses('default')->layout('master.layout');
            // return Theme::view('core.dashboard.vendor.index');
        }        
        

    }


    public function pos_sales_list()
    {
    
        return Theme::view('core.pos-sale.list');
    }
    public function order_sales_list()
    {
        return Theme::view('core.order-sale.list');
    }
    public function sales_report_list()
    {
    
        return Theme::view('core.report.sale-report.list');
    }
    public function product_sale_list()
    {
        return Theme::view('core.report.product-sale.list');
    }
    public function product_graphs_list()
    {    
        return Theme::view('core.report.graphs.list');
    }
    public function sales_sold_list()
    {
        return Theme::view('core.report.sales-sold.list');
    }

    public function category_add()
    {
        return Theme::view('core.category.add');
    }
    public function category_list()
    {
    
        return Theme::view('core.category.list');
    }
    public function product_add()
    {
        return Theme::view('core.product.add');
    }
    public function product_list()
    {
    
        return Theme::view('core.product.list');
    }
    public function user_list()
    {
    
        return Theme::view('core.users.main.s-index');
    }
    public function user_add()
    {
    
        return Theme::view('core.users.main.s-create');
    }
    public function roll_list()
    {
    
        return Theme::view('core.users.roles.s-index');
    }
    public function roll_add()
    {
    
        return Theme::view('core.users.roles.s-create');
    }
    public function menu_list()
    {
    
        return Theme::view('core.menu.list');
    }
    public function menu_add()
    {
    
        return Theme::view('core.menu.create');
    }
    public function sadmin_list()
    {
    
        return Theme::view('core.sadmin.list');
    }
    public function sadmin_add()
    {
    
        return Theme::view('core.sadmin.create');
    }
    public function permission()
    {
    
        return Theme::view('core.users.permissions.s-create');
    }

    public function profile()
    {
    
        return Theme::view('core.profile.view');
        
    }
    public function settings()
    {
    
        return Theme::view('core.system.settings');
        
    }
    public function errorlog()
    {
    
        return Theme::view('core.system.errorlog');
        
    }
    public function activites()
    {
    
        return Theme::view('core.system.activites');
        
    }

    public function countries()
    {
    
        return Theme::view('core.system.countries');
        
    }
    public function currencies()
    {
    
        return Theme::view('core.system.currencies');
        
    }
    public function languges()
    {
    
        return Theme::view('core.system.languges');
        
    }
    public function locations()
    {
    
        return Theme::view('core.system.locations');
        
    }
    public function mail_templates()
    {
    
        return Theme::view('core.system.mail_templates');
        
    }
    public function mealtimes()
    {
    
        return Theme::view('core.system.mealtimes');
        
    }
    public function oder_statuses()
    {
    
        return Theme::view('core.system.oder_statuses');
        
    }
    public function security_questions()
    {
    
        return Theme::view('core.system.security_questions');
        
    }
    public function all_unit()
    {
    
        return Theme::view('core.system.all_unit');
        
    }
    public function payments()
    {
    
        return Theme::view('core.system.payments');
        
    }    
   
    /*
      Later Replace Orginal Module Place
    */
    


    public function system(){

        return Theme::view('core.system.all_unit');
    }   
    public function system_information(){

        return Theme::view('core.system.all_unit');
    }   
    public function log(){
        return Theme::view('core.system.all_unit');
    }   
    public function warnings(){
        return Theme::view('core.system.all_unit');
    }   
    public function maintenance(){
        return Theme::view('core.system.all_unit');
    }   
    public function message_queue(){
        return Theme::view('core.system.all_unit');
    }    



    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('core.core::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core.core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('core.core::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
