<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Orders;
use Modules\Core\Entities\Payments;
use Modules\Core\Entities\sale_history;
use Theme;
Theme::uses('default')->layout('master.layout');

class CashierController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {   

        return Theme::view('cashier.index');
    }
    public function payment_list(Request $request)
    {

        $orders = Orders::where('created_at', '>=', \Carbon\Carbon::now()->subDay()->format('Y-m-d'))->where('status_id', 6)->orderBy('created_at', 'desc')->get();
        return Theme::view('cashier.index',compact('orders'));
    }
    public function customer_info($id){
        $floors = Orders::where("order_id", $id )->first();

            return json_encode($floors);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('cashier.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        $order_data = array(
            'status_id' =>  6,
            'payment_status' => 1
        );        
        $order = Orders::where('order_id', $request->casher_order)->update($order_data);
        
        $payments_data = array(
            'serialized'=> serialize($request->payment_method),
            'payment_status' => 1
        );        
        
        $payments = Payments::where('order_id', $request->casher_order)->update($payments_data); 
        
        $role = \Auth::user()->roles->first()->name;
        
        $sale_history = sale_history::create([
                'vendor_id' => 1,
                'order_id' => $request->casher_order,
                'staff_id' => \Auth::user()->id,
                'status_id' => 6,
                'notify' => 1,
                'status_type' => 'order',
                'hrole' => $role,
                'payment_status' => 1
        ]);

        return redirect()->route('cashier')->with(['success' => 'The order payment cleared']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('core::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
