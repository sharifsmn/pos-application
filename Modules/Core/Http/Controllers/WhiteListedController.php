<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Authorize;
use Theme;
Theme::uses('default')->layout('master.layout');


class WhiteListedController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $white_lists = Authorize::where('authorized', 1)->get();

        return Theme::view('configuration.security.whitelist.index',compact('white_lists'));
    }

    public function BlackListed()
    {
        $white_lists = Authorize::where('authorized', 0)->get();

        return Theme::view('configuration.security.whitelist.blacklist',compact('white_lists'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('configuration.security.whitelist.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Iptables::create($request->all());
        
        return redirect()->route('whitelisted.index');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('core::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $Iptables = Iptables::findOrFail($id);
        $Iptables->delete();
        return redirect()->route('whitelisted.index');

    }
}
