<?php

namespace Modules\Core\Http\Controllers\Configuration;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Spatie\TranslationLoader\LanguageLine;
use Theme;
Theme::uses('default')->layout('master.layout');
class TranslationsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
        $translations = LanguageLine::orderBy('group', 'DESC')->get();
        return Theme::view('configuration.translations.index',compact('translations'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        
        return Theme::view('configuration.translations.create');        
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if($request->group){
            $group = $request->group;
        }else{
            $group = $request->select_group;
        }
        LanguageLine::create([
           'group' => $group,
           'key' => $request->key,
           'text' => $request->text,
        ]);
        $status = array(
                    'type' => 'Create',
                    'success' => 'Translation added successfully'
                    );
         return redirect()->route('translations.index')->with($status);        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $translation = LanguageLine::findOrFail($id);
        return Theme::view('configuration.translations.edit',compact('translation'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {

        if($request->group){
            $group = $request->group;
        }else{
            $group = $request->select_group;
        }        
        $translations = LanguageLine::findOrFail($id);
        $translations->update([
           'group' => $group,
           'key' => $request->key,
           'text' => $request->text,
        ]);        
        $status = array(
                    'type' => 'Update',
                    'success' => 'Translations updated successfully'
                    );
         return redirect()->route('translations.index')->with($status);        
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $tax_codes = LanguageLine::findOrFail($id)->delete();
        $status = array(
                    'type' => 'Delete',
                    'success' => 'Translations deleted successfully'
                    );
         return redirect()->route('translations.index')->with($status);

    }
}
