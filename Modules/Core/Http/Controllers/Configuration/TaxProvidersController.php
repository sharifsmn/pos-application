<?php

namespace Modules\Core\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Tax_codes;
use Modules\Core\Entities\Settings;
use Theme;
Theme::uses('default')->layout('master.layout');

class TaxProvidersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $taxes = Tax_codes::all();
        return Theme::view('configuration.tax.tax_providers.index',compact('taxes'));
    }

    public function default_update(Request $request){
        
        Settings::where('option_key','global_tax')->update([
                            'option_value' => $request->default_taxes
                        ]);
        $status = array(
                    'type' => 'Update',
                    'success' => 'Taxes make default successfully'
                    );
         return redirect()->route('taxes.index')->with($status);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('configuration.tax.tax_providers.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        Tax_codes::create($request->all());

        $status = array(
                    'type' => 'Create',
                    'success' => 'Taxes added successfully'
                    );
         return redirect()->route('taxes.index')->with($status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $tax = Tax_codes::findOrFail($id);
        return Theme::view('configuration.tax.tax_providers.edit',compact('tax'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $tax_codes = Tax_codes::findOrFail($id);
        $tax_codes->update($request->all());
        $status = array(
                    'type' => 'Update',
                    'success' => 'Taxes updated successfully'
                    );
         return redirect()->route('taxes.index')->with($status);

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

        $tax_codes = Tax_codes::findOrFail($id)->delete();
        $status = array(
                    'type' => 'Delete',
                    'success' => 'Taxes deleted successfully'
                    );
         return redirect()->route('taxes.index')->with($status);

    }
}
