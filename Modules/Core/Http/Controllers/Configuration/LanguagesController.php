<?php

namespace Modules\Core\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Languages;
use Modules\Core\Entities\Settings;
use Theme;
Theme::uses('default')->layout('master.layout');
class LanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $languages = Languages::all();

        return Theme::view('configuration.languages.index',compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('configuration.languages.creat');
        
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Languages::create($request->all());

        $status = array(
                    'type' => 'Create',
                    'success' => 'Languages added successfully'
                    );
         return redirect()->route('languages.index')->with($status);

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $language = Languages::findOrFail($id);        
        return Theme::view('configuration.languages.edit',compact('language'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $tax_codes = Languages::findOrFail($id);
        $tax_codes->update($request->all());
        $status = array(
                    'type' => 'Update',
                    'success' => 'Languages updated successfully'
                    );
         return redirect()->route('languages.index')->with($status);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $tax_codes = Languages::findOrFail($id)->delete();
        $status = array(
                    'type' => 'Delete',
                    'success' => 'Languages deleted successfully'
                    );
         return redirect()->route('languages.index')->with($status);

    }

    public function default_update(Request $request){
        Settings::where('option_key','default_language')->update([
                            'option_value' => $request->default_language
                        ]);
        $status = array(
                    'type' => 'Update',
                    'success' => 'Languages make default successfully'
                    );
         return redirect()->route('languages.index')->with($status);

    }
}
