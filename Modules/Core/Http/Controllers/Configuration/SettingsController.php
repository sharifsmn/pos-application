<?php

namespace Modules\Core\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Settings;
use File;
use Auth;
use Intervention\Image\Facades\Image as Image;
use Theme;
Theme::uses('default')->layout('master.layout');
class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $settings = Settings::all();
        return Theme::view('configuration.all_settings.index',compact('settings'));
    }

    public function default_update(Request $request)
    {
        $site_name = Settings::where('option_key','site_name')->first();
        
        if($site_name){
            Settings::where('option_key','site_name')->update([
                            'option_value' => $request->site_name,
                        ]);        
        }else{
            Settings::create([
                'vendor_id' => $request->vendor_id,
                'option_key' => 'site_name',
                'option_value' => $request->site_name,
            ]);
        }

        $site_name = Settings::where('option_key','site_email')->first();
        
        if($site_name){
            Settings::where('option_key','site_email')->update([
                            'option_value' => $request->site_email,
                        ]);        
        }else{
            Settings::create([
                'vendor_id' => $request->vendor_id,
                'option_key' => 'site_email',
                'option_value' => $request->site_email,
            ]);
        }

        $site_name = Settings::where('option_key','meta_keywords')->first();
        if($site_name){
            Settings::where('option_key','meta_keywords')->update([
                            'option_value' => $request->meta_keywords,
                        ]);        
        }else{
            Settings::create([
                'vendor_id' => $request->vendor_id,
                'option_key' => 'meta_keywords',
                'option_value' => $request->site_email,
            ]);
        }

        $site_name = Settings::where('option_key','meta_description')->first();
        if($site_name){
            Settings::where('option_key','meta_description')->update([
                            'option_value' => $request->meta_description,
                        ]);        
        }else{
            Settings::create([
                'vendor_id' => $request->vendor_id,
                'option_key' => 'meta_description',
                'option_value' => $request->site_email,
            ]);
        }          
        $site_name = Settings::where('option_key','copy_rights')->first();
        if($site_name){
            Settings::where('option_key','copy_rights')->update([
                            'option_value' => $request->copy_rights,
                        ]);        
        }else{
            Settings::create([
                'vendor_id' => $request->vendor_id,
                'option_key' => 'copy_rights',
                'option_value' => $request->site_email,
            ]);
        }                   

        if($request->file('site_logo')){
            $filename = $this->site_logo($request->file('site_logo'));

            $site_name = Settings::where('option_key','site_logo')->first();
            if($site_name){
                Settings::where('option_key','site_logo')->update([
                                'option_value' => $filename,
                            ]);        
            }else{
                Settings::create([
                    'vendor_id' => $request->vendor_id,
                    'option_key' => 'site_logo',
                    'option_value' => $filename,
                ]);
            } 
        }
        $status = array(
                'type' => 'Update',
                'success' => 'Configuration updated successfully'
                );
        return redirect()->route('settings.index')->with($status);

    }

    public function site_logo($image){
        $filename  = time() . '.' . $image->getClientOriginalExtension();
        $thumbs  = 'thumbs-'. \Auth::user()->id . '.' . $image->getClientOriginalExtension();

        $dir = public_path('uploads/vendor_id_1/site_logo/');
        File::makeDirectory($dir, 0777, true, true);

        $path = public_path('uploads/vendor_id_1/site_logo/' . $filename); //change the vendor id later
        Image::make($image->getRealPath())->save($path);
        

        return $filename;
    }        
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('core::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('core::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
