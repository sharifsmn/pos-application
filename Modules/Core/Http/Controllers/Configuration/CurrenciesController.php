<?php

namespace Modules\Core\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Currencies;
use PragmaRX\Countries\Package\Countries;
use Theme;
Theme::uses('default')->layout('master.layout');
class CurrenciesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $currencies = Currencies::all();
        return Theme::view('configuration.currencies.index',compact('currencies'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('configuration.currencies.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        Currencies::create($request->all());
        $status = array(
                    'type' => 'Add',
                    'success' => 'Currency added successfully'
                    );
         return redirect()->route('currencies.index')->with($status);

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $currency = Currencies::findOrFail($id); 
        return Theme::view('configuration.currencies.edit',compact('currency') );

    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $currencies = Currencies::findOrFail($id);
        $currencies->update($request->all());
        $status = array(
            'type' => 'Update',
            'success' => 'Currencies updated successfully'
            );
        return redirect()->route('currencies.index')->with($status);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $currencies = Currencies::findOrFail($id)->delete();
        $status = array(
                    'type' => 'Delete',
                    'success' => 'Currencies deleted successfully'
                    );

        return redirect()->route('currencies.index')->with($status);        
    }
}
