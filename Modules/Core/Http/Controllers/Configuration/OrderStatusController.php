<?php

namespace Modules\Core\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Order_status;
use Modules\Core\Entities\Tablestatus;
use Theme;
use Session;
Theme::uses('default')->layout('master.layout');
class OrderStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $orders = Order_status::all();
        return Theme::view('configuration.order_status.index',compact('orders'));
    }

    public function table_status()
    {
        $tablestatus = Tablestatus::all();
        return Theme::view('configuration.table_status.index',compact('tablestatus'));
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('configuration.order_status.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $order = Order_status::create($request->all());
        
        $status = array(
                    'type' => 'Add',
                    'success' => 'Order Added successfully'
                    );        
        return redirect()->route('order_status.index')->with($status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($order_id)
    {
       $order = Order_status::findOrFail($order_id);

        
        return Theme::view('configuration.order_status.edit',compact('order'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $order_status = Order_status::findOrFail($id);
        $order_status->update($request->all());

        $status = array(
                    'type' => 'Update',
                    'success' => 'Order Updated successfully'
                    );        
        return redirect()->route('order_status.index')->with($status);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)    
    {
        $order = Order_status::findOrFail($id)->delete();

        $status = array(
            'type' => 'Delete',
            'success' => 'Order deleted successfully'
            );   
        return redirect()->route('order_status.index')
                        ->with($status);
    }
}
