<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Categories;
use Theme;
Theme::uses('default')->layout('master.layout');

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $categories = Categories::all();
        return Theme::view('core.products.categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */


    public function create()
    {

        return Theme::view('core.products.categories.form');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:categories|max:255',
            'description' => 'required',
            'priority' => 'required',
        ]);
        Categories::create($request->all());
        $status = array(
                'type' => 'Add',
                'success' => 'Category added successfully'
                );
        return redirect()->route('categories.index')->with($status);

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $category = Categories::findOrFail($id);
        return Theme::view('core.products.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'priority' => 'required',
        ]);
                
        $tax_codes = Categories::findOrFail($id);
        $tax_codes->update($request->all());
        $status = array(
                    'type' => 'Update',
                    'success' => 'Category updated successfully'
                    );
         return redirect()->route('categories.index')->with($status);

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $tax_codes = Categories::findOrFail($id)->delete();
        $status = array(
                    'type' => 'Delete',
                    'success' => 'Taxes deleted successfully'
                    );
         return redirect()->route('categories.index')->with($status);        
    }
}
