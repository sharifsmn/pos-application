<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Menu;
use Modules\Core\Entities\Categories;
use File;
use Auth;
use Intervention\Image\Facades\Image as Image;
use Theme;
Theme::uses('default')->layout('master.layout');

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $menus = Menu::all();
        return Theme::view('core.products.menus.index',compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function form()
    {

        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('core.products.menus.form');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'menu_name' => 'required|unique:menus|max:255',
            'menu_description' => 'required',
         ]);     

        
        $menu_save = [
            'vendor_id' => 1,
            'menu_name'=> $request->input('menu_name'),
            'menu_description'=> $request->input('menu_description'),
            'menu_price'=> $request->input('menu_price'),
            'menu_category_id'=> $request->input('menu_category'),
            'menu_priority' => $request->input('menu_priority'),
            'tax' => $request->input('menu_tax'),
            'time' => $request->input('menu_time'),
            'status' => $request->input('status'),
        ];
        if($request->file('menu_photo')){
            $image = $request->file('menu_photo');
            $filename = $this->menu_image($image);
                        
            $menu_save['menu_photo'] = $filename;             
        }

        if($request->file('slider_photo')){
            $slider_insert = array();
            $i = 0;
            foreach($request->file('slider_photo') as $key => $val):
                $slider_insert[$i]['photo'] = $this->menu_image($val['photo']);
                $slider_insert[$i]['status'] = 1;
                $i++;
            endforeach;
            $menu_save['slider'] = serialize($slider_insert);   
            

        }         
        

        Menu::create($menu_save); 
        $status = array(
            'type' => 'Update',
            'success' => 'Configuration updated successfully'
        );

        return redirect()->route('menus.index')->with($status);
    }

    public function menu_image($image){

        $filename  = rand (10,100000) . '.' . $image->getClientOriginalExtension();
        
        $dir = public_path('uploads/vendor_id_1/menu/');
        File::makeDirectory($dir, 0777, true, true);

        $path = public_path('uploads/vendor_id_1/menu/' . $filename); 

        Image::make($image->getRealPath())->save($path);

        return $filename;
    }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $menu = Menu::findOrFail($id);
        return Theme::view('core.products.menus.edit',compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        
        $menu_save = [
            'vendor_id' => 1,
            'menu_name'=> $request->input('menu_name'),
            'menu_description'=> $request->input('menu_description'),
            'menu_price'=> $request->input('menu_price'),

            'menu_category_id'=> $request->input('menu_category'),
            'menu_priority' => $request->input('menu_priority'),
            'status' => $request->input('status'),
        ]; 
        if($request->file('menu_photo')){
            $image = $request->file('menu_photo');
            $filename = $this->menu_image($image);
                        
            $menu_save['menu_photo'] = $filename;             
        }
        
        if($request->file('slider_photo')){
            $slider_insert = array();
            $i = 0;

                //dd($request->file('slider_photo'));
            foreach($request->file('slider_photo') as $key => $val):
                $image = $this->menu_image($val['photo']);
                $slider_insert[]['photo'] = $image;
                $i++;
            endforeach;
            $menu_save['slider'] = serialize($slider_insert);   
            

        }          
        Menu::where('menu_id',$id)->update($menu_save);
        $status = array(
                'type' => 'Update',
                'success' => 'Configuration updated successfully'
                );

        return redirect()->route('menus.index')->with($status);         
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $tax_codes = Menu::findOrFail($id)->delete();
        $status = array(
                    'type' => 'Delete',
                    'success' => 'Taxes deleted successfully'
                    );
         return redirect()->route('menus.index')->with($status);        
    }
}
