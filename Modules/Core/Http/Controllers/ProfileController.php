<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Profiles;
use File;
use Auth;
use Intervention\Image\Facades\Image as Image;
use Theme;
Theme::uses('default')->layout('master.layout');

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $profile = Profiles::where('user_id', \Auth::user()->id)->first();

        if($profile){
            return Theme::view('core.profile.view',compact('profile'));
        }else{
            return Theme::view('core.profile.create');
        }
        //return view('core::index');
    
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */

    public function profile()
    {
        return Theme::view('core.profile.form');
    }

    public function forntend_profile()
    {
        Theme::uses('front')->layout('master.layout');
        $profile = Profiles::where('user_id', \Auth::user()->id)->first();

        if($profile){
            $profile = Profiles::where('user_id', \Auth::user()->id)->first();
            return Theme::view('frontend.profile.profile',compact('profile'));
        }else{

            return Theme::view('frontend.profile.create');
        }


        
    }


    public function create()
    {
        return view('core::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        $save = [
                    'user_id' => \Auth::user()->id,
                    'vendor_id' => \Auth::user()->vendor_id,
                    'firstname'=> $request->input('firstname'),
                    'lastname'=> $request->input('lastname'),
                    'company'=> $request->input('company'),
                    'designation'=> $request->input('company'),
                    'bio'=> $request->input('bio'),
                    'location'=> $request->input('location'),
                    'notes'=> $request->input('notes'),
                ];      
        if($request->file('profile_image')){
          $filename = $this->profile_image($request->file('profile_image'));
          $save['profile_image'] = $filename;
        }

        

        Profiles::create($save);        
        return redirect()->back();        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('core::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $profile = Profiles::findOrFail($id);
        $save = [
        'user_id' => \Auth::user()->id,
        'vendor_id' => \Auth::user()->vendor_id,
        'firstname'=> $request->input('firstname'),
        'lastname'=> $request->input('lastname'),
        'company'=> $request->input('company'),
        'designation'=> $request->input('company'),
        'bio'=> $request->input('bio'),
        'location'=> $request->input('location'),
        'notes'=> $request->input('notes'),
        ];      
        if($request->file('profile_image')){
          $filename = $this->profile_image($request->file('profile_image'));
          $save['profile_image'] = $filename;
        }
        $profile->update($save);
     
        return redirect()->back();        

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function profile_image($image){
        $filename  = time() . '.' . $image->getClientOriginalExtension();

        $dir = public_path('uploads/vendor_id_1/profile/');
        File::makeDirectory($dir, 0777, true, true);

        $path = public_path('uploads/vendor_id_1/profile/' . $filename); //change the vendor id later
        Image::make($image->getRealPath())->save($path);
        

        return $filename;
    }
}
