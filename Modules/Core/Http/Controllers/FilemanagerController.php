<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Theme;
Theme::uses('default')->layout('master.layout');

class FilemanagerController extends Controller
{
    public function index()
    {
        return Theme::view('filemanager.index');
    }

}
