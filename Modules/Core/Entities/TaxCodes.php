<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Tax_codes extends Model
{
    protected $fillable = [
                            'vendor_id',
                            'tax_code_name',
                            'tax_rate',
                          ];
}
