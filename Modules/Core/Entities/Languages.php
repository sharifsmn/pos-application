<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Languages extends Model
{
    protected $fillable = [
                           'vendor_id',
                           'code', 
                           'name',
                           'status',
                           'can_delete',  
                          ];
}
