<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
	protected $primaryKey = 'floor_id';
    protected $fillable = ['vendor_id', 'location_id','floor_name','square_size', 'size'];

    public function locations()
    {
        return $this->belongsTo('Modules\Core\Entities\Location','location_id');
    }   


}
