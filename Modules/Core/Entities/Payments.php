<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
  protected $fillable = [
                          'vendor_id',
                          'table_id',
                          'order_id', 
                          'serialized',
                          'payment_status',
                          ];
}
