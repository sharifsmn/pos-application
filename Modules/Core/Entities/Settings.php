<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
                            'vendor_id',
                            'option_key', 
                            'option_value',  
                            
                          ];
}
