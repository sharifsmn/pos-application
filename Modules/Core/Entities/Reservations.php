<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Reservations extends Model
{
    protected $primaryKey = 'reservation_id';
    protected $fillable = [
                            'vendor_id',
                            'location_id',
                            'floor_id',
                            'table_id',
                            'customer_id',
                            'name',
                            'email',
                            'telephone',
                            'reserve_time',
                            'reserve_date',
                            'status'  
                          ];
                          
}

