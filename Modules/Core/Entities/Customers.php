<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $fillable = [
    						'vendor_id',
							'first_name',
							'last_name',
							'email',
							'telephone',
							'address_id',
							'ip_address',
							'status',
							'cart'
						];
}
