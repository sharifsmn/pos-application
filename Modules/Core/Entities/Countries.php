<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
	protected $primaryKey = 'country_id';
    protected $fillable = ['vendor_id','country_name','iso_code_2','iso_code_2','iso_code_3','status','flag'];
}
