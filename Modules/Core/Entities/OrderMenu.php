<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Order_menu extends Model
{
  protected $primaryKey = 'order_menu_id';
  protected $fillable = [
                          'vendor_id',
                          'table_id',
                          'order_id', 
                          'menu_id',
                          'name',
                          'quantity',
                          'price',
                          'subtotal',
                          'notes',                        
                          'tax',
                          'orginal_price',
                          ];
}
