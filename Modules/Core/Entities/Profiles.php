<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
    protected $fillable = ['user_id','vendor_id','firstname','lastname','company','designation','bio','location','notes','profile_image'];

	public function user()
    {
        return $this->belongsTo('App\User');
   	}    

}
