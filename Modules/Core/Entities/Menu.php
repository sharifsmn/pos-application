<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $primaryKey = 'menu_id';
    protected $fillable = [
                            'vendor_id',
                            'menu_name',
                            'menu_description',
                            'menu_price',
                            'menu_photo',
                            'menu_category_id',
                            'tax','time',
                            'slider',
                            'status',
                            'menu_priority'
                          ];
}
