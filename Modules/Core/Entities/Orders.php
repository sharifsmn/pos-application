<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
  protected $primaryKey = 'order_id';
  protected $fillable = [
                          'vendor_id',
                          'table_id',
                          'first_name', 
                          'last_name',
                          'email',
                          'telephone',
                          'total_items',
                          'status_id',
                          'ip_address',
                          'user_agent',
                          'assignee_id',
                          'invoice_no',
                          'payment',
                          'order_total',
                          'cart',
                          'order_type',
                          'payment_status',
                        ];
  protected $dates = ['created_at', 'updated_at'];                        
}
