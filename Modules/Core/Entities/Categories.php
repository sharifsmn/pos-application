<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
	protected $primaryKey = 'category_id';
    protected $fillable = ['vendor_id','name','description','priority','status'];
}
