<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Iptables extends Model
{
    protected $fillable = ['ip_address', 'whitelisted'];
    
    protected $casts = [
     'whitelisted' => 'boolean',
    ];  

 	public static function whitelist_ips(){
 		return Iptables::where('whitelisted',1);
 	}

  	public static function blacklist_ips(){
 		return Iptables::where('whitelisted',0);
 	}

}
