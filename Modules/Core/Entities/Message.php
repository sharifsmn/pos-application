<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
    						'vendor_id',
    						'sender_id',
    						'send_type',
    						'recipient',
    						'subject',
    						'body',
    						'status',
    						];
}
