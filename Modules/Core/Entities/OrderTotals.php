<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Order_totals extends Model
{
  protected $primaryKey = 'order_total_id';
  protected $fillable = [
                          'vendor_id',
                          'table_id',
                          'order_id', 
                          'subtotal',
                          'discount',
                          'global_tax',
                          'submitted_total',
                          'total_item_count',
                          ];
}
