<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Currencies extends Model
{
    protected $fillable = [
                          'vendor_id',
                          'country_id',
                          'currency_name',
                          'currency_code',
                          'currency_symbol',
                          'currency_rate',
                          'currency_status',
    ];
}
