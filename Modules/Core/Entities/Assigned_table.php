<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Assigned_table extends Model
{
  protected $fillable = [
                          'vendor_id',
                          'table_id', 
                          'order_id',
                          'takeout_id',
                          'status',
                          ];
}
