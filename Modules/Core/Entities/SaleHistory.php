<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class sale_history extends Model
{
    protected $fillable = [
              'vendor_id',
              'order_id',
              'staff_id',
              'status_id',
              'notify',
              'status_type',
              'comment',
              'hrole',
              'payment_status',
                
    ];
}
