<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Order_status extends Model
{
     protected $primaryKey = 'status_id';
    protected $fillable = [
    					'vendor_id',
							'status_name',
							'status_comment',
							'notify_customer',
							'status_for',
							'status_color',
							'created_at',
							'updated_at'
						];
}
