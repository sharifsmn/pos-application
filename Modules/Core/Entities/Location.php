<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	protected $primaryKey = 'location_id';
	
    protected $fillable = ['vendor_id',
            'location_name',
            'location_address',
            'location_phone',
            'description',
            'status,'
            ];

}
