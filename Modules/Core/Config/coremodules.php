<?php

return [
    'dashboard' => [
        'sales-graph' => 'View list',
        'latest-orders' => 'View list',
        'latest-members' => 'View list',
        'recently-added-products' =>'View list',
    ],
    'widgets' => [
        'view-widgets' => 'View list',
        'create-widgets' => 'Create',
        'update-widgets' => 'Edit',
        'delete-widgets' => 'Delete',
    ],
    'files' => [
        'view-files' => 'View list',
        'create-file' => 'Create',
        'update-file' => 'Edit',
        'delete-file' => 'Delete',
    ],
    'roles' => [
        'view-roles' => 'View list',
        'create-role' => 'Create',
        'update-role' => 'Edit',
        'delete-role' => 'Delete',
    ],
    'translations' => [
        'view-translations' => 'View list',
        'create-translation' => 'Create',
        'update-translation' => 'Edit',
        'delete-translation' => 'Delete',
    ],
    'users' => [
        'view-users' => 'View list',
        'create-user' => 'Create',
        'update-user' => 'Edit',
        'delete-user' => 'Delete',
    ],
    'users list' => [
        'view-users-list' => 'View list',
        'create-user-list' => 'Create',
        'update-user-list' => 'Edit',
        'delete-user-list' => 'Delete',
    ], 
    'restaurant' => [
        'view-restaurant' => 'View list',
        'create-restaurant' => 'Create',
        'update-restaurant' => 'Edit',
        'delete-restaurant' => 'Delete',
    ], 
    'table' => [
        'view-table' => 'View list',
        'create-table' => 'Create',
        'update-table' => 'Edit',
        'delete-table' => 'Delete',
    ], 
    'live kitchen' => [
        'view-live-kitchen' => 'View list',
    ], 
    'waiting line' => [
        'view-waiting-line' => 'View list',
        'create-waiting-line' => 'Create',
        'update-waiting-line' => 'Edit',
        'delete-waiting-line' => 'Delete',
    ],  
    'takeout' => [
        'view-takeout' => 'View list',
        'done-takeout' => 'Create',
    ],  
    'location' => [
        'view-location' => 'View list',
        'create-location' => 'Create',
        'add-floor' => 'Add',
        'update-location' => 'Edit',
        'delete-location' => 'Delete',
    ], 
    'product' => [
        'view-product' => 'View list',
        'create-product' => 'Create',
        'update-product' => 'Edit',
        'delete-product' => 'Delete',
    ], 
    'categories' => [
        'view-categories' => 'View list',
        'create-categories' => 'Create',
        'update-categories' => 'Edit',
        'delete-categories' => 'Delete',
    ], 
    'menus' => [
        'view-menus' => 'View list',
        'create-menus' => 'Create',
        'update-menus' => 'Edit',
        'delete-menus' => 'Delete',
    ],    
    'sales' => [
        'view-sales' => 'View list',
        'create-sales' => 'Create',
        'update-sales' => 'Edit',
        'delete-sales' => 'Delete',
    ],     
    'customer' => [
        'view-customer' => 'View list',
        'create-customer' => 'Create',
        'update-customer' => 'Edit',
        'delete-customer' => 'Delete',
    ],       
    'reporting' => [
        'view-reporting' => 'View list',
        'create-reporting' => 'Create',
        'update-reporting' => 'Edit',
        'delete-reporting' => 'Delete',
    ],       
    'configuration' => [
        'view-configuration' => 'View list',
        'create-configuration' => 'Create',
        'update-configuration' => 'Edit',
        'delete-configuration' => 'Delete',
    ],   
];
?>
