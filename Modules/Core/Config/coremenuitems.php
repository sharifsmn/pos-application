<?php

return [
    'menu-item' => [
        'view-menu-item' => 'View list',
        'create-menu-item' => 'Create',
        'update-menu-item' => 'Edit',
        'delete-menu-item' => 'Delete',
    ],    
    
];
