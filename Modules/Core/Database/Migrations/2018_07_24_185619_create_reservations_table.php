<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('reservation_id');
            $table->integer('vendor_id');            
            $table->integer('location_id');
            $table->integer('table_id');
            $table->integer('guest_num');
            $table->integer('occasion_id');
            $table->integer('customer_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('telephone');
            $table->text('comment');
            $table->timestamp('reserve_time')->nullable();
            $table->timestamp('reserve_date')->nullable();
            $table->timestamp('date_added')->nullable();
            $table->timestamp('date_modified')->nullable();
            $table->integer('assignee_id');
            $table->addColumn('tinyInteger', 'notify', ['length' => 1,'default'=> 0]);            
            $table->ipAddress('ip_address');
            $table->string('user_agent');
            $table->addColumn('tinyInteger', 'status', ['length' => 1,'default'=> 0]);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
