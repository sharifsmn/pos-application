<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('currency_id');
            $table->integer('vendor_id');
            $table->integer('country_id');
            $table->string('currency_name',50);
            $table->string('currency_code',3);
            $table->string('currency_symbol',3);
            $table->decimal('currency_rate',15,8);
            $table->addColumn('tinyInteger', 'symbol_position', ['length' => 4]);
            $table->char('thousand_sign',1);
            $table->char('decimal_sign',1);
            $table->char('decimal_position',1);
            $table->string('iso_alpha2',2);
            $table->string('iso_alpha3',2);
            $table->integer('iso_numeric');
            $table->string('flag',6);
            $table->text('format');
            $table->addColumn('tinyInteger', 'status', ['length' => 1,'default'=> 0]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
