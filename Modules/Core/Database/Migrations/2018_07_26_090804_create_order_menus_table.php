<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_menus', function (Blueprint $table) {
            $table->increments('order_menu_id');
            $table->integer('vendor_id');
            $table->integer('order_id');
            $table->integer('menu_id');
            $table->string('name');
            $table->integer('quantity');
            $table->decimal('price', 15, 4);
            $table->decimal('subtotal', 15, 4);
            $table->text('option_values');
            $table->text('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_menus');
    }
}
