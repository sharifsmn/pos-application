<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->increments('status_id');
            $table->integer('vendor_id');
            $table->string('status_name', 50);
            $table->text('status_comment');
            $table->addColumn('tinyInteger', 'notify_customer', ['length' => 1,'default'=> 0]);            
            $table->string('status_for', 10);
            $table->string('status_color', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_statuses');
    }
}
