<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->integer('menu_option_id');
            $table->integer('option_id');
            $table->integer('menu_id');
            $table->integer('required');
            $table->addColumn('tinyInteger', 'default_value_id', ['length' => 4,'default'=> 0]);
            $table->text('option_values');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_options');
    }
}
