<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('vendor_id');
            $table->integer('customer_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('telephone');
            $table->integer('location_id');
            $table->integer('address_id');
            $table->string('cart');
            $table->integer('total_items');
            $table->text('comment');
            $table->string('payment', 32);
            $table->string('order_type', 32);
            $table->timestamp('date_added')->nullable();
            $table->timestamp('date_modified')->nullable();
            $table->string('order_time');
            $table->string('order_date');
            $table->decimal('order_total', 15, 4);
            $table->integer('status_id');
            $table->ipAddress('ip_address');
            $table->string('user_agent');
            $table->addColumn('tinyInteger', 'notify', ['length' => 1,'default'=> 0]);            
            $table->integer('assignee_id');
            $table->integer('invoice_no');
            $table->string('invoice_prefix');
            $table->timestamp('invoice_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
