<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('message_id');
            $table->integer('vendor_id');
            $table->integer('sender_id');
            $table->timestamp('date_added');
            $table->string('send_type');
            $table->string('recipient');
            $table->text('subject');
            $table->text('body');
            $table->addColumn('tinyInteger', 'status', ['length' => 1,'default'=> 0]);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
