<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkingHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->integer('location_id');
            $table->integer('weekday');
            $table->timestamp('opening_time')->nullable();
            $table->timestamp('closing_time')->nullable();
            $table->addColumn('tinyInteger', 'status', ['length' => 1,'default'=> 0]);  
            $table->string('type', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('working_hours');
    }
}
