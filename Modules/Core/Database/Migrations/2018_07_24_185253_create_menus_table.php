<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('menu_id');
            $table->integer('vendor_id');
            $table->string('menu_name');
            $table->text('menu_description');
            $table->decimal('menu_price', 15, 4);
            $table->string('menu_photo')->nullable();
            $table->integer('menu_category_id');
            $table->addColumn('tinyInteger', 'status', ['length' => 1,'default'=> 0]);
            $table->integer('menu_priority');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
