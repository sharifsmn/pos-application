<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_items', function (Blueprint $table) {
            $table->increments('sale_id');
            $table->integer('vendor_id');
            $table->integer('stored_item_id');
            $table->integer('sale_item_id');
            $table->integer('qty');
            $table->string('name');
            $table->string('description');
            $table->string('tax_id');
            $table->string('tax');
            $table->decimal('unit', 15, 4);
            $table->decimal('price', 15, 4);
            $table->integer('refundqty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_items');
    }
}
