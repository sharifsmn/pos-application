<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function tinyInteger($column, $autoIncrement = false, $unsigned = false)
    {
        return $this->addColumn('tinyInteger', $column, compact('autoIncrement', 'unsigned'));
    }

    public function up()
    {
        Schema::create('coupons_histories', function (Blueprint $table) {
            $table->increments('coupon_history_id');
            $table->integer('vendor_id');            
            $table->integer('coupon_id');
            $table->integer('order_id');
            $table->integer('customer_id');
            $table->string('code', 15);
            $table->decimal('min_total', 15, 4)->nullable();
            $table->string('amount', 15, 4)->nullable();
            $table->timestamp('date_used');
            $table->addColumn('tinyInteger', 'status', ['length' => 4]);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons_histories');
    }
}
