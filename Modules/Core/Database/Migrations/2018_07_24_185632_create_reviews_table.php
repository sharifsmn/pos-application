<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('review_id');
            $table->integer('vendor_id');
            $table->integer('customer_id');
            $table->integer('sale_id');
            $table->string('sale_type', 50);
            $table->string('author');
            $table->integer('location_id');
            $table->integer('quality');
            $table->integer('delivery');
            $table->integer('service');
            $table->integer('review_text');
            $table->timestamp('date_added')->nullable();
            $table->addColumn('tinyInteger', 'status', ['length' => 1,'default'=> 0]);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
