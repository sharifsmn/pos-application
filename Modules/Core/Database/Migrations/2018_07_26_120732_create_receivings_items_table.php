<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivingsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receivings_items', function (Blueprint $table) {
            $table->increments('receiving_id');
            $table->integer('item_id');
            $table->string('description');
            $table->string('serialnumber');
            $table->integer('line');
            $table->decimal('quantity_purchased', 15, 2);
            $table->decimal('item_cost_price', 15, 2);
            $table->decimal('item_unit_price', 15, 2);
            $table->decimal('discount_percent', 15, 2);
            $table->integer('item_location');
            $table->decimal('receiving_quantity', 15, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receivings_items');
    }
}
