<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('location_id');
            $table->integer('vendor_id');
            $table->string('location_name');
            $table->string('location_email',128);
            $table->text('description');
            $table->string('location_address_1',128);
            $table->string('location_address_2',128)->nullable();
            $table->string('location_city',128);
            $table->string('location_state',128);
            $table->string('location_postcode',20);
            $table->integer('location_country_id');
            $table->string('location_telephone',50);
            $table->float('location_lat',10,6);
            $table->float('location_lng',10,6);
            $table->integer('location_radius')->nullable();
            $table->tinyInteger('offer_delivery');
            $table->tinyInteger('offer_collection');
            $table->integer('delivery_time');
            $table->integer('last_order_time');
            $table->integer('reservation_time_interval');
            $table->integer('reservation_stay_time')->nullable();
            $table->tinyInteger('status')->default('0');
            $table->integer('collection_time');
            $table->text('options')->nullable();
            $table->string('location_image')->nullable();            
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
