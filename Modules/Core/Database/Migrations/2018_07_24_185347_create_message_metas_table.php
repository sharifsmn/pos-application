<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_metas', function (Blueprint $table) {
            $table->increments('message_meta_id');
            $table->integer('vendor_id');
            $table->string('message_id');
            $table->addColumn('tinyInteger', 'state', ['length' => 1]);
            $table->addColumn('tinyInteger', 'status', ['length' => 1,'default'=> 0]);
            $table->addColumn('tinyInteger', 'deleted', ['length' => 4]);
            $table->string('item', 50);
            $table->text('value');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_metas');
    }
}
