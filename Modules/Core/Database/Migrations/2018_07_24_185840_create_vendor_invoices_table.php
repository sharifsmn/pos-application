<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_invoices', function (Blueprint $table) {
            $table->increments('vendor_invoice_id');
            $table->string('vendor_id');       
            $table->string('amount');      
            $table->timestamp('timestamp');       
            $table->string('method');      
            $table->text('payment_details');
            $table->addColumn('tinyInteger', 'status', ['length' => 1,'default'=> 0]);      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_invoices');
    }
}
