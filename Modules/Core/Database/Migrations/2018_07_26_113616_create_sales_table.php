<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->string('ref');
            $table->string('type');
            $table->string('channel');
            $table->integer('data');
            $table->integer('user_id');
            $table->integer('device_id');
            $table->integer('location_id');
            $table->integer('customer_id');
            $table->decimal('discount', 15, 4);
            $table->decimal('rounding', 10, 2);
            $table->decimal('total', 15, 2);
            $table->decimal('balance',15, 2);
            $table->addColumn('tinyInteger', 'status', ['length' => 1,'default'=> 0]);
            $table->bigInteger('processdt');
            $table->bigInteger('duedt');
            $table->timestamp('dt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
