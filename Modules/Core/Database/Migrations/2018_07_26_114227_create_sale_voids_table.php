<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleVoidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_voids', function (Blueprint $table) {
            $table->increments('sale_id');
            $table->integer('vendor_id');
            $table->integer('user_id');
            $table->integer('device_id');
            $table->integer('location_id');
            $table->string('reason');
            $table->string('method');
            $table->integer('amount');
            $table->string('items');
            $table->addColumn('tinyInteger', 'void', ['length' => 1,'default'=> 0]);
            $table->bigInteger('processdt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_voids');
    }
}
