<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('expense_id');
            $table->integer('vendor_id');
            $table->timestamp('date')->nullable();
            $table->decimal('amount', 15, 2);
            $table->string('payment_type', 50);
            $table->integer('expense_category_id');
            $table->text('description');
            $table->integer('employee_id');
            $table->addColumn('tinyInteger', 'deleted', ['length' => 1,'default'=> 0]);            
            $table->string('supplier_name');
            $table->string('supplier_tax_code');
            $table->decimal('tax_amount', 15, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
