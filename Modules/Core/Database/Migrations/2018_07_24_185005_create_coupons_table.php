<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');            
            $table->integer('coupon_id');
            $table->string('code');
            $table->char('type');
            $table->decimal('discount', 15, 4)->nullable();
            $table->decimal('min_total', 15, 4)->nullable();
            $table->integer('redemptions');
            $table->integer('customer_redemptions')->default(0);
            $table->text('description');
            $table->boolean('status');
            $table->timestamp('date_added');
            $table->char('validity');
            $table->timestamp('fixed_date')->nullable();
            $table->timestamp('fixed_from_time')->nullable();
            $table->timestamp('fixed_to_time')->nullable();
            $table->timestamp('period_start_date')->nullable();
            $table->timestamp('period_end_date')->nullable();
            $table->string('recurring_every');
            $table->string('recurring_from_time')->nullable();
            $table->string('recurring_to_time')->nullable();
            $table->string('order_restriction');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
