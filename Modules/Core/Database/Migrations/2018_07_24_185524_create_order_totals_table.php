<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_totals', function (Blueprint $table) {
            $table->increments('order_total_id');
            $table->integer('vendor_id');
            $table->integer('order_id');
            $table->string('code', 50);
            $table->string('title');
            $table->decimal('value', 15, 4);
            $table->addColumn('tinyInteger', 'priority', ['length' => 1,'default'=> 0]);            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_totals');
    }
}
