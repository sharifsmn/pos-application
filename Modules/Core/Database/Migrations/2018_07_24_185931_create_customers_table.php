<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->integer('customer_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('salt');
            $table->string('telephone');
            $table->integer('address_id');
            $table->integer('customer_group_id');
            $table->ipAddress('ip_address');
            $table->timestamp('date_added')->nullable();
            $table->addColumn('tinyInteger', 'status', ['length' => 1,'default'=> 0]);
            $table->text('cart');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
