<?php

namespace Modules\Report\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Orders;
use Modules\Core\Entities\sale_history;
use App\User;
use Theme;
Theme::uses('default')->layout('master.layout');

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('report::index');
    }



    public function kitchen()
    {
        $orders = Orders::all();

        return Theme::view('reports.kitchen.index',compact('orders'));
    }

    public function waiter()
    {
        Theme::uses('default')->layout('graphs.waiter-layout');
        return Theme::view('reports.waiter.index');
    }
    public function location()
    {
        Theme::uses('default')->layout('graphs.location-layout');
        return Theme::view('reports.location.index');
    }

    public function sales()
    {
        return Theme::view('reports.sales.index');
    }
    public function graphs()
    {
        Theme::uses('default')->layout('graphs.graphs-layout');
        return Theme::view('reports.graphs.index');
    }
    public function sales_manager_sold()
    {
        return Theme::view('reports.sales_manager_sold.index');
    }

    public function low_stock()
    {
        return Theme::view('reports.low_stock.index');
    }    

    public function customer()
    {
        return Theme::view('reports.customer.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('report::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('report::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('report::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
