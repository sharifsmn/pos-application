<?php
Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
	Route::group(['middleware' => ['web','auth'], 'prefix' => 'report', 'namespace' => 'Modules\Report\Http\Controllers'], function()
	{
	    Route::get('/kitchen', 'ReportController@kitchen');
	    Route::get('/waiter', 'ReportController@waiter');
	    Route::get('/sales', 'ReportController@sales');
	    Route::get('/graphs', 'ReportController@graphs');
	    Route::get('/sales_manager_sold', 'ReportController@sales_manager_sold');
	    Route::get('/low_stock', 'ReportController@low_stock');
	    Route::get('/customer', 'ReportController@customer');
	    
	    Route::get('/receipt', 'InvoiceController@receipt');
	    Route::resource('/invoice', 'InvoiceController');

	    // Restaurant Location Report
	    Route::get('/waiter', 'ReportController@waiter');
	    Route::post('/waiter', 'ReportController@waiter');
	    Route::get('/location', 'ReportController@location');

	});
});
