<?php
Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    Route::group(['middleware' => ['web','auth'], 'prefix' => 'customer', 'namespace' => 'Modules\Customer\Http\Controllers'], function()
    {
        Route::get('/member_customer', 'CustomerController@member_customer');
        Route::get('/customer_not_saved', 'CustomerController@customer_not_saved');
        Route::get('/customer_saved', 'CustomerController@customer_saved');
        Route::get('/', 'CustomerController@index');
        Route::resource('all', 'CustomerController');
    });
});

