<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Customers;
use Session;
use Theme;
Theme::uses('default')->layout('master.layout');

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $customers = Customers::all();
        return Theme::view('customers.main.index',compact('customers'));
    }
    public function create()
    {
        return Theme::view('customers.main.create');

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $customers = Customers::create($request->all());
        return redirect('customer/member_customer')->with('success','Application deleted successfully');

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('customers::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($customer_id)
    {
        $customers = Customers::findOrFail($customer);
        return Theme::view('customers.main.edit',compact('customers'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $customers = Customers::findOrFail($id);
        $customers->update($request->all());
        Session::flash('flash_message', 'Table updated successfully!');
        return redirect()->route('all.index');

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

        $customers = Customers::findOrFail($id)->delete();
        return redirect()->route('all.index')
                        ->with('success','Application deleted successfully');

    }

    public function member_customer()
    {
        $customers = Customers::all();
        return Theme::view('customers.member_customer.index',compact('customers'));
    }

    public function customer_not_saved()
    {
        return Theme::view('customers.customer_not_saved.index');
    }

    public function customer_saved()
    {
        return Theme::view('customers.customer_saved.index');
    }
}
