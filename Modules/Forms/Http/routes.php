<?php

Route::group(['middleware' => 'web', 'prefix' => 'forms', 'namespace' => 'Modules\Forms\Http\Controllers'], function()
{
    Route::get('/', 'FormsController@index');
});
