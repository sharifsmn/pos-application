<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'page', 'namespace' => 'Modules\Page\Http\Controllers'], function()
{
    Route::get('/', 'PageController@index');
});
