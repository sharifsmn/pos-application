<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'locations', 'namespace' => 'Modules\Locations\Http\Controllers'], function()
{
    Route::get('/', 'LocationsController@index');
});
