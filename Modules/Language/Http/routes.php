<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'language', 'namespace' => 'Modules\Language\Http\Controllers'], function()
{
    Route::get('/', 'LanguageController@index');
});
