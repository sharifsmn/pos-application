<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaitinglinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waitinglines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->integer('capacity');
            $table->string('name');
            $table->date('waiting')->nullable();
            $table->string('qouted')->nullable();
            $table->text('notes');
            $table->boolean('notify')->default(0);
            $table->integer('assigned_table');
            $table->boolean('status')->default(0);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waitinglines');
    }
}
