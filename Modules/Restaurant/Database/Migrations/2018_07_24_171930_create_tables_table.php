<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tables', function (Blueprint $table) {
            $table->increments('table_id');          
            $table->integer('vendor_id');
            $table->integer('location_id');
            $table->integer('floor_id');
            $table->string('table_name');
            $table->string('min_capacity');
            $table->string('max_capacity');
            $table->tinyInteger('status')->default('1');

            $table->timestamps();
        });

        Schema::create('table_status', function (Blueprint $table) {
            $table->increments('id');          
            $table->integer('vendor_id');
            $table->integer('location_id');
            $table->string('name');
            $table->string('color');
            $table->tinyInteger('status')->default('1');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tables');
        Schema::dropIfExists('table_status');
    }
}
