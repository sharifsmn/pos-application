<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealtimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mealtimes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mealtime_id');
            $table->string('mealtime_name');
            $table->string('start_time');
            $table->string('end_time');
            $table->string('mealtime_status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mealtimes');
    }
}
