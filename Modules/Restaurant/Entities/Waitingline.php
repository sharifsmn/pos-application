<?php

namespace Modules\Restaurant\Entities;

use Illuminate\Database\Eloquent\Model;

class Waitingline extends Model
{
    protected $fillable = ['vendor_id', 'capacity', 'name','waiting','qouted','status','notes','notify','assigned_table','takeout'];

}
