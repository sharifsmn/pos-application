<?php

namespace Modules\Restaurant\Entities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class Table extends Model
{
	protected $primaryKey = 'table_id';
    protected $fillable = ['vendor_id', 'location_id', 'floor_id','table_name','min_capacity','max_capacity','table_status'];
}
