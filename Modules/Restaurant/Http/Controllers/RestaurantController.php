<?php

namespace Modules\Restaurant\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Restaurant\Entities\Table;
use Session;
use Theme;
Theme::uses('default')->layout('master.layout');

use App\Rules\UniqueTable;


class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        


        if(!empty($request->input('restaurant')) && !empty($request->input('floor_id'))){
            $tables = Table::where([
                ['location_id', '=', $request->input('restaurant')],
                ['floor_id', '=', $request->input('floor_id')],
                ])->get();            
        }else{

            $tables = Table::all();
        }

        return Theme::view('restaurant.table.main.index',compact('tables'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('restaurant.table.main.create');

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'table_name' => ['required', new UniqueTable($request->all())],
            'location_id' => 'required',
            'floor_id' => 'required',
            'max_capacity' => 'required',
        ]);        

        $user = Table::create($request->all());
        return redirect()->route('tables.index')->with('success','Application deleted successfully');

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        return Theme::view('restaurant.table.main.create');


    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($table_id)
    {
        $table = Table::findOrFail($table_id);
        return Theme::view('restaurant.table.main.edit',compact('table'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'table_name' => 'required|max:255',
            'location_id' => 'required',
            'floor_id' => 'required',
            'max_capacity' => 'required',
        ]);

        $table = Table::findOrFail($id);
        $table->update($request->all());
        return redirect()->route('tables.index')
                        ->with('success','Table updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

        $table = Table::findOrFail($id)->delete();
        return redirect()->route('tables.index')
                        ->with('success','Table deleted successfully');

    }




    public function kitchen_live(){
        return Theme::view('restaurant.table.kitchen_live.index');
    }

    public function waiting_line(){
        return Theme::view('dashboard.superadmin.index');
    }

    public function takeout(){
        return Theme::view('dashboard.superadmin.index');
    }     

}
