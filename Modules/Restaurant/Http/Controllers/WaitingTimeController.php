<?php

namespace Modules\Restaurant\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Restaurant\Entities\Waitingline;
use Theme;
Theme::uses('default')->layout('master.layout');

class WaitingTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $waitinglines = Waitingline::where('status', 1)->get();
        return Theme::view('restaurant.waiting_time.index',compact('waitinglines'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('restaurant.waiting_time.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Waitingline::create($request->all());
        return redirect()->route('waitingtime.index')->with('success','Customer added successfully');

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        Waitingline::find($id)->update([
            'status' => 0,
            'takeout' => 1,
        ]);
        
        $status = array(
                'type' => 'Update',
                'success' => 'Configuration updated successfully'
                );
        return redirect()->route('waitingtime.index')->with($status);

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('restaurant::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
