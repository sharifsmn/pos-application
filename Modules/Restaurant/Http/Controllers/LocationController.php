<?php

namespace Modules\Restaurant\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Location; //will be moved in the restaurant
use Modules\Core\Entities\Floor; 
use Modules\Restaurant\Entities\Table; 
use Modules\Core\Entities\Reservations;
use Modules\Core\Entities\Orders;
use Modules\Core\Entities\Order_menu;
use Modules\Core\Entities\Order_totals;
use Modules\Core\Entities\Assigned_table;
use Modules\Core\Entities\Payments;
use Modules\Restaurant\Entities\Waitingline;
use Modules\Core\Entities\sale_history;

use PragmaRX\Countries\Package\Countries;
use Modules\Core\Entities\Countries as Savecountry;
use Theme;
Theme::uses('default')->layout('master.layout');
use App\Notifications\SiteNotification as SiteNotification;
use Illuminate\Support\Facades\Notification;
class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $locations = Location::all();
        return Theme::view('restaurant.location.index',compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        $countries = new Countries();
        //$count = $countries->all();

        $all_countries = $countries->all()->pluck('name');
        // foreach($count as $key => $val){
            
        //     $country = $val['name']['common'];
        //     $iso2 = !empty($val['iso_a2']) ? $val['iso_a2'] : 'no';

        //     $iso3 = !empty($val['iso_a3']) ? $val['iso_a3'] : 'no';
        //     $flag = str_replace('/var/www/works/contributed/outsource/sharif/restaurant_management/projects', '', $val['flag']['svg_path']);
        //     if(strlen($iso2) == 2){
        //         Savecountry::create(
        //             [
        //                 'vendor_id' => 1,
        //                 'country_name' => $country,
        //                 'iso_code_2' => $iso2,
        //                 'iso_code_3'=> $iso3, 
        //                 'status'=> 1,
        //                 'flag'=> $flag,
        //             ]
        //         );
        //     }

        // }
        return Theme::view('restaurant.location.create', compact('all_countries') );
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'location_name' => 'required|unique:locations|max:255',
            'description' => 'required',
            'location_address' => 'required',
            'location_phone' => 'required',

        ]);
        $vendor_id = $request->input('vendor_id');
        $location = Location::create([
            'vendor_id' => $vendor_id,
            'location_name' => $request->input('location_name'),
            'location_address' => $request->input('location_address'),
            'location_phone' => $request->input('location_phone'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
            ]);

        // here is something goes in the settings page.
        if(!empty($location)){
            Floor::create([
                'vendor_id' => $vendor_id,
                'location_id' => $location->location_id, 
                'floor_name' => 'Floor 1',
                'size' => '10', 
                'square_size' => '2400', 
            ]);

            $user  = \Auth::user();
            $message = "Saved location successfully";
            Notification::send(\App\User::first(), new SiteNotification($user,$message));            
            
        }
        return redirect()->route('location.index')->with('success','Location added successfully');

    }


        public function getFloors($id) {
            $floors = Floor::where("location_id", $id )->pluck("floor_name","floor_id");

            return json_encode($floors);

        }

        public function gettables($id) {
            $floors = Table::where("floor_id", $id )->pluck("table_name","table_id");

            return json_encode($floors);

        }        

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('restaurant::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $location = Location::findOrFail($id);

        return Theme::view('restaurant.location.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {

            $request->validate([
                'location_name' => 'required|max:255',
                'description' => 'required',
            ]);
        
            $table = Location::findOrFail($id);
            $table->update($request->all());
            $user  = \Auth::user();
            $message = "Saved location successfully";
            Notification::send(\App\User::first(), new SiteNotification($user,$message));            

            return redirect()->route('location.index')->with('success','Location updated successfully');;

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        // // // delete all floor
        $floors = Floor::where('location_id', $id)->get();
        foreach($floors as $key => $floor){
            Floor::findOrFail($floor->floor_id)->delete();
        }
        //delete all tables

        $tables = Table::where('location_id', $id)->get();
        foreach($tables as $key => $table){

            $orders = Orders::where('table_id', $table->table_id)->get();
            foreach($orders as $key => $order){
                
                $order_menus = order_menu::where('order_id', $order->order_id)->get();
                //delete order menu
                foreach($order_menus as $order_menu){
                    order_menu::findOrFail($order_menu->order_menu_id)->delete();
                }

                $order_totals = Order_totals::where('order_id', $order->order_id)->get();
                //delete order menu
                foreach($order_totals as $order_total){
                    Order_totals::findOrFail($order_total->order_total_id)->delete();
                }                

                $assigned_tables = Assigned_table::where('order_id', $order->order_id)->get();
                //delete order menu
                foreach($assigned_tables as $assigned_table){
                    Assigned_table::findOrFail($assigned_table->id)->delete();
                }
                $payments = Payments::where('order_id', $order->order_id)->get();
                //delete order menu
                foreach($payments as $payment){
                    Payments::findOrFail($payment->id)->delete();
                }
                $waitinglines = Waitingline::where('order_id', $order->order_id)->get();
                //delete order menu
                foreach($waitinglines as $waitingline){
                    Waitingline::findOrFail($waitingline->id)->delete();
                }

                $sale_histories = sale_history::where('order_id', $order->order_id)->get();
                //delete order menu
                foreach($sale_histories as $sale_history){
                    sale_history::findOrFail($sale_history->id)->delete();
                }                

                //delete orders
                Orders::findOrFail($order->order_id)->delete();
            }
            // delete table             
            Table::findOrFail($table->table_id)->delete();
        }

        //delete all reseravations
        $reservations = Reservations::where('location_id', $id)->get();
        foreach($reservations as $key => $reservation){
            Reservations::findOrFail($reservation->reservation_id)->delete();
        } 

        $table = Location::findOrFail($id)->delete();
        return redirect()->route('location.index')
                        ->with('success','Location deleted with its all data successfully');

    }
}
