<?php

namespace Modules\Restaurant\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Floor;

use Theme;
Theme::uses('default')->layout('master.layout');
use App\Rules\UniqueFloor;

class FloorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {

        $floors = Floor::where('location_id',$request->input('restaurant_id'))->get();
        return Theme::view('restaurant.floor.index',compact('floors'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('restaurant.floor.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'floor_name' => ['required', new UniqueFloor($request->all())],
        ]);        
        $user = Floor::create($request->all());
        return redirect()->route('floor.index', ['restaurant_id' => $request->input('location_id')])->with('success','Floor added successfully');

    }

    /**
     * Show the specified resource.
     * @return Response(
          */
         public function show(Request $request, $id)
         {
            $floors = Floor::where('location_id',$request->input('restaurant_id'))->get();
            $infloor = Floor::findOrFail($id);
            return Theme::view('restaurant.floor.edit',compact('floors','infloor'));
        }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $floors = Floor::where('location_id',$request->input('restaurant_id'))->get();
        $infloor = Floor::findOrFail($id);
        return Theme::view('restaurant.floor.edit',compact('floors','infloor'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
            $table = Floor::findOrFail($id);
            $table->update($request->all());
               return redirect()->route('floor.edit', ['floor_id'=>$id,'restaurant_id' => $request->input('restaurant_id')])->with('success','Floor added successfully');

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $table = Floor::findOrFail($id)->delete();

        return redirect()->route('floor.index', ['restaurant_id' => $request->input('restaurant_id')])->with('success','Floor added successfully');

    }
}
