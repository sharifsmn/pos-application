<?php
Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    Route::group(['middleware' => ['web','auth'], 'prefix' => 'restaurant', 'namespace' => 'Modules\Restaurant\Http\Controllers'], function()
    {
        // Route::get('/kitchen_live', 'RestaurantController@kitchen_live');
        
        Route::get('/takeout', 'RestaurantController@takeout');
        
        Route::get('/', 'RestaurantController@index');
        
        Route::get('/table', 'RestaurantController@table');
        Route::resource('tables', 'RestaurantController')->names([
                    'create' => 'table.create',
                    'store' => 'table.store',
                    'edit'  => 'table.edit',
                    'update'  => 'table.update',
                    'destroy'  => 'table.destroy',

                ]);

        // Route::get('/waitingtime/create', 'WaitingTimeController@create');
        Route::resource('waitingtime', 'WaitingTimeController');

        // Route::get('/takeout/create', 'TakeOutController@create');
        Route::resource('takeout', 'TakeOutController');

        Route::resource('location', 'LocationController');

        Route::resource('floor', 'FloorController', ['parameters' => [
                    'index' => 'restaurant_id'
                ]]);
        
        Route::get('/location/get/{order_id}', [
                        'as' => 'locafolor', 'uses' => 'LocationController@getFloors'
                        ]); 
        Route::get('/floor/get/{order_id}', [
                        'as' => 'locafolor', 'uses' => 'LocationController@gettables'
                        ]);    


    });
});