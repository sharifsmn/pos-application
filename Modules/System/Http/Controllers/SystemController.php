<?php

namespace Modules\System\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Larinfo;
use Theme;
Theme::uses('default')->layout('master.layout');

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('system::index');
    }
    public function system_information()
    {
        
        $getHostIpinfo = Larinfo::getHostIpinfo();
        $getClientIpinfo = Larinfo::getClientIpinfo();
        $getServerInfoSoftware = Larinfo::getServerInfoSoftware();
        $getServerInfoHardware = Larinfo::getServerInfoHardware();
        $getUptime = Larinfo::getUptime();
        $getServerInfo = Larinfo::getServerInfo();
        $getDatabaseInfo = Larinfo::getDatabaseInfo();
        //dd($getDatabaseInfo);

        return Theme::view('system.system_information.index',compact('getHostIpinfo','getClientIpinfo','getServerInfoSoftware','getServerInfoHardware','getUptime','getServerInfo','getDatabaseInfo'));
    }
    public function log()
    {
        return Theme::view('system.log.index');
    }
    public function warnings()
    {
        return Theme::view('system.warnings.index');
    }
    public function maintenance()
    {
        return Theme::view('system.maintenance.index');
    }
    public function message_queue()
    {
        return Theme::view('system.message_queue.index');
        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('system::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('system::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('system::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
