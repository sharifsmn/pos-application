<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'system', 'namespace' => 'Modules\System\Http\Controllers'], function()
{
    Route::get('/', 'SystemController@index');
    Route::get('/system_information', 'SystemController@system_information');
    Route::get('/log', 'SystemController@log');
    Route::get('/warnings', 'SystemController@warnings');
    Route::get('/maintenance', 'SystemController@maintenance');
    Route::get('/message_queue', 'SystemController@message_queue');
});
