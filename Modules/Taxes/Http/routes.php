<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'taxes', 'namespace' => 'Modules\Taxes\Http\Controllers'], function()
{
    Route::get('/', 'TaxesController@index');
});
