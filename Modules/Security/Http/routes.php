<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'security', 'namespace' => 'Modules\Security\Http\Controllers'], function()
{
    Route::get('/', 'SecurityController@index');
});
