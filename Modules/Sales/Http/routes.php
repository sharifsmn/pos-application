<?php
Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    Route::group(['middleware' => ['web','auth'], 'prefix' => 'sales', 'namespace' => 'Modules\Sales\Http\Controllers'], function()
    {
        Route::get('/', 'SalesController@index');

        Route::get('/nopadioder', 'SalesController@nopadioder');
        Route::get('/products_never_purchased', 'SalesController@products_never_purchased');
        Route::get('/bestsellers', 'SalesController@bestsellers');
        Route::get('/current_shopping_carts', 'SalesController@current_shopping_carts');
        Route::get('/recurring_payments', 'SalesController@recurring_payments');
        Route::get('/shipments', 'SalesController@shipments');
        Route::get('/ReturnRequest', 'SalesController@ReturnRequest');

        // orders
        // Route::get('/orders/edit', 'OrdersController@formedit');
        // Route::get('/orders/form', 'OrdersController@form');
        Route::post('/orders/formupdate/', [
                    'as' => 'orders_update', 'uses' => 'OrdersController@orders_update'
                    ]);        
        Route::resource('orders', 'OrdersController'); 

        // Pos Sales
        Route::get('/pos_sales/edit', 'PosSalesController@formedit');
        Route::get('/pos_sales/form', 'PosSalesController@form');
        Route::resource('/pos_sales', 'PosSalesController'); 

        // reservation
        Route::post('/reservation/formupdate/', [
                    'as' => 'reservation_update', 'uses' => 'ReservationController@reservation_update'
                    ]);
        Route::resource('reservations', 'ReservationController'); 

    });
});
