<?php

namespace Modules\Sales\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Orders;
use Modules\Core\Entities\sale_history;
use Theme;
Theme::uses('default')->layout('master.layout');

use App\Notifications\SiteNotification as SiteNotification;
use Illuminate\Support\Facades\Notification;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $orders = Orders::all();
        return Theme::view('sales.orders_sales.index',compact('orders'));
    }
    public function invoice($id)
    {
     return Theme::view('reports.invoice.index');   
    }    
    

    public function orders_update(Request $request){
        
        
        Orders::where('order_id', $request->order_id)->update(['status_id'=> $request->order_status]);

        $sale_history = sale_history::create([
                'vendor_id' => 1,
                'order_id' => $request->order_id,
                'staff_id' => \Auth::user()->id,
                'status_id' => $request->order_status,
                'notify' => 1,
                'status_type' => 'order',
            ]);

        $order_status = \Modules\Core\Entities\Order_status::where('status_id',$request->order_status)->first();


        $users = \App\User::permission('view-live-kitchen')->where('status',1)->get();
        
        $current_user = \Auth::user();
        
        $message = "Order $order_status->status_name Successfully" . ' Order Id: '. $request->order_id;
        
        foreach($users as $user){
            Notification::send($user, new SiteNotification($current_user,$message));
        }

        $waiter = \App\User::where('status',1)->role('waiter')->get();
        foreach($waiter as $user){
            Notification::send($user, new SiteNotification($current_user,$message));
        }


        $status = array(
                    'type' => 'Update',
                    'success' => 'Orders Updated'
                    );

        return redirect()->route('orders.index')->with($status); 
    }

    public function form()
    {
        
    }    
    public function formedit()
    {
        return Theme::view('sales.orders_sales.edit');
    }    

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('sales.orders_sales.form');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('sales::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $orders = Orders::findOrFail($id);
        return Theme::view('sales.orders_sales.edit',compact('orders'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

        $tax_codes = Orders::findOrFail($id)->delete();
        $status = array(
                    'type' => 'Delete',
                    'success' => 'Orders deleted successfully'
                    );
         return redirect()->route('orders.index')->with($status);

    }
}
