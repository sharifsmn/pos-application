<?php

namespace Modules\Sales\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Floor;
use Modules\Core\Entities\Menu;
use Modules\Restaurant\Entities\Table;
use Modules\Core\Entities\Categories;
use Modules\Core\Entities\Customers;
use Modules\Core\Entities\Orders;
use Modules\Core\Entities\Order_menu;
use Modules\Core\Entities\Order_totals;
use Modules\Core\Entities\Assigned_table;
use Modules\Core\Entities\Payments;


use Theme;
Theme::uses('default')->layout('master.layout');

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('sales::index');
    }

    public function nopadioder()
    {
        $orders = Orders::where('status_id', 3)->get();

        return Theme::view('sales.nopadioder.index',compact('orders'));
    }

    public function products_never_purchased()
    {
        $categories = Categories::all();
        return Theme::view('sales.products_never_purchased.index',compact('categories'));
    }

    public function bestsellers()
    {
        $categories = Categories::all();
        
        return Theme::view('sales.bestsellers.index',compact('categories'));
    }

    public function current_shopping_carts()
    {
        return Theme::view('sales.current_shopping_carts.index');
    }

    public function recurring_payments()
    {
        return Theme::view('sales.recurring_payments.index');
    }

    public function shipments()
    {
        return Theme::view('sales.shipments.index');
    }

    public function ReturnRequest()
    {
        return Theme::view('sales.ReturnRequest.index');
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sales::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('sales::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('sales::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
