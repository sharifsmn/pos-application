<?php

namespace Modules\Sales\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Reservations;
use Modules\Core\Entities\Customers;
use Theme;
Theme::uses('default')->layout('master.layout');

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return Theme::view('sales.reservations.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return Theme::view('sales.reservations.form');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'location_id' => 'required',
        ]);     

        $customers = Customers::create([
            'vendor_id' => 1,
            'first_name' => $request->c_name,    
            'last_name' => $request->c_name,    
            'email' => $request->c_email,    
            'telephone' => $request->c_phone,    
            'ip_address' => $request->ip(),    
            'status' => 1,    

        ]);
        if($customers){
            $reservation = Reservations::create([
                    'vendor_id' => 1,
                    'location_id' => $request->location_id,
                    'floor_id' => $request->floor_id,
                    'table_id' => $request->table_id,
                    'customer_id' => $customers->id,
                    'name' => $request->c_name,
                    'email' => $request->c_email,
                    'telephone' => $request->c_phone,
                    'reserve_time' => date('h:i:a', strtotime($request->r_time)),
                    'reserve_date' => date('Y-m-d', strtotime($request->r_date)),
                    'status' => 7,
            ]);
        }
        $status = array(
                    'type' => 'Delete',
                    'success' => 'Reservation Added'
                    );
         return redirect()->route('reservations.index')->with($status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('sales::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {   
        $reservation = Reservations::findOrFail($id);
        return Theme::view('sales.reservations.edit', compact('reservation'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
    $reservation = Reservations::where('reservation_id',$id)->update([
            'vendor_id' => 1,
            'location_id' => $request->location_id,
            'floor_id' => $request->floor_id,
            'table_id' => $request->table_id,
            'name' => $request->c_name,
            'email' => $request->c_email,
            'telephone' => $request->c_phone,
            'reserve_time' => date('h:i:a', strtotime($request->r_time)),
            'reserve_date' => date('Y-m-d', strtotime($request->r_date)),
    ]);

       $customers = Customers::where('id', $request->customer_id)->update([
            'vendor_id' => 1,
            'first_name' => $request->c_name,    
            'last_name' => $request->c_name,    
            'email' => $request->c_email,    
            'telephone' => $request->c_phone,    
            'ip_address' => $request->ip(),    
            'status' => 1,    

        ]);


        $status = array(
                    'type' => 'Update',
                    'success' => 'Reservation Updated'
                    );
         return redirect()->route('reservations.index')->with($status);            
    }

    public function reservation_update(Request $request){
        
        Reservations::where('reservation_id',$request->reservation_id)->update(['status'=> $request->reservation_update]);
        return redirect()->route('reservations.index');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

        $tax_codes = Reservations::findOrFail($id)->delete();
        $status = array(
                    'type' => 'Delete',
                    'success' => 'Reservations deleted successfully'
                    );
         return redirect()->route('reservations.index')->with($status);
    }     
}
