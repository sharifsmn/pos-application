<?php

return [
    'blocks' => [
        'view-blocks' => 'View list',
        'create-block' => 'Create',
        'update-block' => 'Edit',
        'delete-block' => 'Delete',
    ],
    
    'widgets' => [
        'view-widgets' => 'View list',
        'create-widgets' => 'Create',
        'update-widgets' => 'Edit',
        'delete-widgets' => 'Delete',
    ],
    'menus' => [
        'view-menus' => 'View list',
        'create-menu' => 'Create',
        'update-menu' => 'Edit',
        'delete-menu' => 'Delete',
    ],
    'files' => [
        'view-files' => 'View list',
        'create-file' => 'Create',
        'update-file' => 'Edit',
        'delete-file' => 'Delete',
    ],
    'roles' => [
        'view-roles' => 'View list',
        'create-role' => 'Create',
        'update-role' => 'Edit',
        'delete-role' => 'Delete',
    ],
    'translations' => [
        'view-translations' => 'View list',
        'create-translation' => 'Create',
        'update-translation' => 'Edit',
        'delete-translation' => 'Delete',
    ],
    'users' => [
        'view-users' => 'View list',
        'create-user' => 'Create',
        'update-user' => 'Edit',
        'delete-user' => 'Delete',
    ],
    
    
];
?>
