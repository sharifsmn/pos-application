<?php

return [
    'blocks' => [
        'view-blocks' => 'View list',
        'create-block' => 'Create',
        'update-block' => 'Edit',
        'delete-block' => 'Delete',
    ],
];
