<?php

return [
    'name' => 'Core',
    'redirect_to'      => '',   // URL TO REDIRECT IF BLOCKED (LEAVE BLANK TO THROW STATUS)
    'response_status'  => 403,  // STATUS CODE (403, 404 ...)
    'response_message' => 'test'    // MESSAGE (COMBINED WITH STATUS CODE)

];
